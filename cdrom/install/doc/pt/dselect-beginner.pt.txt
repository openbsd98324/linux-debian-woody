
                  `dselect' Documenta��o para Iniciantes
                  --------------------------------------

        St�phane Bortzmeyer e outros <debian-doc@lists.debian.org>

           Michelle Ribeiro (Tradutora) <michelle@focalinux.org>


-------------------------------------------------------------------------------


Resumo
------

     Este documento cont�m um breve tutorial destinado � pessoas que
     estejam usando o `dselect' pela primeira vez.  O `dselect' � uma
     interface em console para o gerenciamento de pacotes Debian.  Ele
     suplementa o Installation Manual for Debian GNU/Linux 3.0
     (http://www.debian.org/releases/woody/i386/install).*


-------------------------------------------------------------------------------


�ndice
------

     1.        Introdu��o

     2.        Ap�s o `dselect' ser carregado
     2.1.      ``Access''
     2.2.      ``Update''
     2.3.      ``Select''
     2.4.      ``Install''
     2.5.      ``Configure''
     2.6.      ``Remove''
     2.7.      ``Quit''

     3.        Algumas Dicas na Conclus�o

     4.        Gloss�rio


-------------------------------------------------------------------------------


1. Introdu��o
-------------

     Novas vers�es deste arquivo poder�o ser encontradas em
     http://www.debian.org/releases/woody/i386/dselect-beginner.

     Este arquivo documenta o `dselect' para usu�rios que n�o tiveram
     contato com este programa e tem como objetivo ajudar a instalar a
     Debian com sucesso.  Ele n�o tenta explicar tudo e dessa forma, quando
     voc� entrar pela primeira vez no `dselect', leia as telas de ajuda.

     Se est� ansioso para ver seu Debian rodando o mais r�pido poss�vel,
     ent�o voc� n�o deveria usar o `dselect' :-) O processo de instala��o
     da Debian permite a execu��o do `tasksel', que fornece algumas tarefas
     generalizadas que voc� pode selecionar e concluir a instala��o.

     O `dselect' � usado para selecionar quais pacotes voc� deseja instalar
     (atualmente h� em torno de 8300 pacotes na Debian 3.0).  Ser�
     executado durante a instala��o e por ser muito poderoso e um pouco
     complexo, pode ser usado para o bem ou para o mal; Dessa forma, algum
     conhecimento pr�vio � altamente recomendado.  O uso descuidado do
     `dselect' pode danificar seu sistema.

     O `dselect' guiar� voc� durante o processo de instala��o dos pacotes,
     atrav�s dos passos abaixo:

        * Escolher o m�todo de acesso a ser utilizado.

        * Atualizar a lista de pacotes dispon�veis, se poss�vel

        * Escolher os pacotes que voc� quer em seu sitema

        * Instalar e atualizar os pacotes desejados

        * Configurar qualquer pacote que ainda n�o tenha sido configurado

        * Remover programas indesejados

     Assim que cada passo for completado com sucesso, ele ir� conduz�-lo
     para o pr�ximo.  Siga esta ordem, sem pular qualquer passo.

     Neste documento, aqui e ali n�s falamos sobre iniciar um outro shell.
     O Linux tem 6 se��es de terminais ou consoles dispon�veis a qualquer
     hora.  Voc� pode alternar entre eles pressionando _Alt-esquerdo-F1_
     at� _Alt-esquerdo-F6_, depois logar em seu novo shell e continuar.  O
     console usado pelo processo de instala��o � o primeiro, ou seja, tty1;
     assim, pressione _Alt-esquerdo-F1_ quando quiser retornar para ele.


-------------------------------------------------------------------------------


2. Ap�s o `dselect' ser carregado
---------------------------------

     Uma vez no `dselect' voc� ver� uma tela como esta:

          Debian `dselect' package handling frontend.
          
             0. [A]ccess    Choose the access method to use.
             1. [U]pdate    Update list of available packages, if possible.
             2. [S]elect    Request which packages you want on your system.
             3. [I]nstall   Install and upgrade wanted packages.
             4. [C]onfig    Configure any packages that are unconfigured.
             5. [R]emove    Remove unwanted software.
             6. [Q]uit      Quit dselect.
          
          [Algumas outras coisas]

     Vamos analisar estes passos, um a um.


2.1. ``Access''
---------------

     A tela ''Access":

dselect - list of access methods
  Abbrev.        Description
  cdrom          Install from a CD-ROM.
* multi_cd       Install from a CD-ROM set.
  nfs            Install from an NFS server (not yet mounted).
  multi_nfs      Install from an NFS server (using the CD-ROM set) (not yet mounted).
  harddisk       Install from a hard disk partition (not yet mounted).
  mounted        Install from a filesystem which is already mounted.
  multi_mount    Install from a mounted partition with changing contents.
  floppy         Install from a pile of floppy disks.
  apt            APT Acquisition [file,http,ftp]

     Aqui, n�s dizemos ao `dselect' onde est�o os pacotes.  Voc� deve
     ignorar a ordem em que os m�todos s�o exibidos e selecionar aquele que
     for mais apropriado para a sua instala��o.  Voc� poder� ter alguns
     m�todos a mais ou a menos, ou ainda notar que eles s�o listados em
     outra ordem; n�o se preocupe.  Na lista seguinte, nos descrevemos os
     diferentes m�todos.

     apt
          Uma das melhores op��es para instala��o a partir de um mirror
          local do arquivo Debian ou a partir da rede.  Este m�todo usa o
          sistema ``apt'' (veja em apt(8)) para resolver as depend�ncias,
          sendo assim o mais agrad�vel.

          A configura��o desse m�todo � direta; voc� pode selecionar
          diferentes localidades, misturando e adaptando `arquivos:' URLs
          (discos locais ou discos montados via NFS), `http:' URLs ou
          `ftp:' URLs.  Contudo, note que as op��es por HTTP ou FTP n�o
          suportam proxies locais que requerem autentica��o.

          Por favor, leia o sources.list(5) manual para maiores informa��es
          sobre o formato do arquivo `/etc/apt/sources.list'.

          Se tiver um servidor proxy para HTTP ou FTP (ou ambos),
          certifique-se de que tenha configurado as vari�veis de ambiente
          `http_proxy' ou `ftp_proxy', respectivamente.  Configure-as em
          seu shell antes de iniciar o dselect, da seguinte maneira:

               # export http_proxy=http://gateway:3128/
               # dselect

     multi_cd
          Grande e poderoso, este m�todo complexo � o meio recomendado para
          instalar uma vers�o recente da Debian de diversos CDs bin�rios.
          Cada um desses pacotes deve conter informa��es sobre seus pacotes
          e de todos os CDs anteriores (no arquivo `Packages.cd').  Antes
          de selecionar este m�todo, certifique-se de que o CD-ROM que
          utilizar� n�o est� montado.  Coloque o �ltimo CD _bin�rio_ do
          conjunto (n�o precisamos dos CDs dos fontes) na unidade de CD e
          responda as seguintes quest�es:

             * CD-ROM drive location

             * Confirmation that you are using a multi-cd set

             * The location of the Debian distribution on the disk(s)

             * [ Possibly ] the location(s) of the Packages file(s)

          Uma vez que tenha atualizado a lista de pacotes dispon�veis e
          selecionado aqueles que deseja instalar, o m�todo multi-cd difere
          do procedimento normal.  Voc� dever� executar o passo "Install"
          para cada um dos CD.  Infelizmente, devido a suas limita��es, o
          dselect n�o est� apto para solicitar o novo disco em cada
          est�gio; A forma de trabalhar com cada disco �:

             * Insira o CD em sua unidade de CD-ROM

             * A partir da tela principal, selecione ``Install''.

             * Aguarde enquanto o dpkg finaliza a instala��o a partir do CD
               (pode ser relatado o sucesso na instala��o ou poss�veis
               erros de instala��o.  N�o se preocupe com isso por
               enquanto).

             * Pressione [_Enter_] para retornar ao menu principal do
               dselect.

             * Repita estes procedimentos com o pr�ximo CD do conjunto...

          Pode ser preciso executar mais de uma vez o procedimento de
          instala��o para cobrir toda a seq��ncia de instala��o dos pacotes
          - alguns pacotes instalados anteriormente podem precisar de
          pacotes que foram instalados depois, para efetuar uma
          configura��o adequada.

          Recomendamos que execute o passo ``Configur'', para terminar a
          instala��o de qualquer pacote que tenha parado neste est�gio.

     multi_nfs, multi_mount
          Similares ao m�todo multi_cd acima e aperfei�oados quanto a c�pia
          com altera��o de m�dia, como, como exemplo, instala��o atr�ves de
          um conjunto multi-cd exportado via NFS do drive de CD-ROM de uma
          outra m�quina

     Disquete
          Voltada para aqueles que n�o t�m CD-ROM ou ou acesso � rede.  N�o
          � indicado se estiver utilizando disquetes do tamanho
          tradicional, mas funcionar� bem com LS/120 ou Zip drives.
          Especifique a localiza��o de sua unidade de disco flex�vel e
          ent�o insira os disquetes.  O primeiro dever� conter o arquivo
          Packages.  Este � um m�todo lento e n�o muito confi�vel, pois
          podem haver problemas de m�dia.

     nfs
          _DEPRECATED METHOD -- use apt or multi_nfs instead.  Only try
          this method if all else fails._

          This is a simple installation method, with simple requirements:
          give it the address of the NFS server, the location of the Debian
          distribution on the server and (maybe) the Packages file(s).
          Then `dselect' will install the various sections in turn from the
          server.  Slow but easy; does not use proper ordering, so it will
          take many runs of the ``Install'' and/or ``Configure'' steps.
          Obviously only appropriate for NFS based installation.

     harddisk
          _DEPRECATED METHOD -- use apt or multi_mount instead.  Only try
          this method if all else fails!_

          Supply the block device of the hard drive partition to use, and
          the locations of the Debian files on that partition, as usual.
          Slow and easy.  Does not use proper ordering, so it will take
          many runs of the ``Install'' and/or ``Configure'' steps.  Not
          recommended, since the ``apt'' method supports this
          functionality, with proper ordering.

     mounted
          _M�TODO N�O RECOMENDADO -- Use o apt em seu lugar.  Somente tente
          este m�todo se todos os demais falharem!_

          Simplesmente especifique as localiza��es dos arquivos Debian em
          seu sistema.  Provavelmente � o m�todo mais f�cil, mas muito
          lento.  N�o utiliza uma sequ�ncia adequada, e por tanto, pode ser
          necess�rio v�rias executa��es dos passos ''Install'' e/ou
          ``Configure''.

     cdrom
          _M�TODO N�O RECOMENDADO -- Use o multi_cd em seu lugar.  Este
          m�todo simplesmente n�o funciona com um conjunto de m�ltiplos
          CDs, como foi inclu�do na Debian 3.0._

          Criado para instala��es com um �nico CD, este m�todo simples ir�
          solicitar a localiza��o de sua unidade de CD-ROM, a localiza��o
          da distribui��o Debian neste disco e ent�o (se necess�rio), a
          localiza��o do arquivo Packages.  Simples mas muito lento.  Como
          n�o usa uma seq��ncia adequada, precisar� de v�rias execu��es dos
          passos ``Install'' e/ou ``Configure''.  N�o recomendado pois ir�
          assumir que a distribui��o est� em um �nico CD-ROM, que n�o � o
          caso.  Utilize o m�todo ``multi_cd'' em seu lugar.

     Se voc� est� encontrando muitos problemas -- talvez o Linux n�o possa
     ver seu CD-ROM, a parti��o NFS n�o est� funcionando ou voc� esqueceu
     em que parti��o est�o seus pacotes -- voc� te, duas op��es:

        * Iniciar outro shell.  Corrija o problema e ent�o retorno ao shell
          principal.

        * Saia do `dselect' e execute-o novamente.  Voc� pode precisar
          reiniciar o computador para resolver alguns problemas.  Tudo bem
          quanto a isto, mas quando retornar ao `dselect' execute-o como
          root.  Ele n�o ser� executado automaticamente ap�s a primeira
          vez.

     Depois de escolher o m�todo de acesso, o `dselect' solicitar� que voc�
     indique o local preciso dos pacotes.  Se voc� n�o fizer isto
     corretamente na primeira vez, pressione _Control-C_ e retorne ao item
     ``Access''.

     Terminado aqui, voc� dever� retornar para a tela principal.


2.2. ``Update''
---------------

     O `dselect' ler� o arquivo `Packages' ou `Packages.gz' do mirror e
     criar� em seu sistema uma base de dados com todos os pacotes
     dispon�veis.  Pode levar alguns minutos enquanto o `dselect' baixa os
     arquivos e processa-os.


2.3. ``Select''
---------------

     Segure seu chap�u.  � aqui que tudo acontece.  O objetivo deste passo
     � selecionar quais os pacotes que voc� quer

     Pressione _Enter_.  Se voc� tem uma m�quina um pouco lenta, fique
     atento pois a tela ir� apagar-se e pode continuar assim por uns 15
     segundos.  Dessa forma, n�o digite nada neste momento.  Seja paciente.

     A primeira coisa que aparece na tela � a p�gina 1 do arquivo de ajuda.
     Voc� pode voltar para este arquivo pressionando _?_ a qualquer hora, a
     partir da tela ``Select''.  Voc� poder� visualizar tela por tela,
     pressionando a tecla _._ (full stop).

     Esteja preparado para gastar uma hora ou mais, pois voc� ter� que
     aprender a lidar com isto e ent�o, fazer tudo corretamente.  Quando
     entrar pela primeira vez na tela ``Select'', n�o fa�a _NENHUMA_
     sele��o -- apenas pressione a tecla _Enter_ veja quais problemas de
     depend�ncia.  Tente corrig�-los.  Se conseguir, volte para a tela
     principal novamente, Sec��o 2.3, ```Select'''.

     Antes de aprofundar-se nisso, Before you dive in, lembre-se dos
     seguintes pontos:

        * Para sair da tela ``Select'' depois de completar todas as
          sele��es, pressione _Enter_.  Isto o levar� para a tela principal
          se n�o houver nenhum problema com sua sele��o.  Se n�o, ser�
          questionado o que fazer com este problema.  Quando estiver
          satisfeito, pressione _Enter_ para sair.

        * Problemas s�o normais e esperados.  Se voc� selecionar o pacote
          <A> e ele depender do pacote <B> para ser executado, ent�o o
          `dselect' ir� notific�-lo sobre o problema e sugerir uma solu��o.
          Se o pacote <A> conflita com o pacote <B>> (ou se s�o mutuamente
          exclusivos), ser� solicitado que voc� escolha um deles.

     Vamos dar uma olhada nas duas primeiras linhas da tela ``Select''.

dselect - main package listing (avail., priority)    mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Inst.ver    Avail.ver   Description

     Este cabe�alho nos relembra de algumas teclas especiais:

     `+'
          Seleciona o pacote para instala��o.

     `='
          Coloca um pacote em "hold" -- �til quando um pacote est�
          corrompido.  Voc� pode reinstalar uma vers�o anterior e colocar a
          atual em espera enquanto aguarda o surgimento de uma nova.  (De
          qualquer forma, estas coisas raramente ocorrem com a vers�o
          est�vel da Debian.)

     `-' Remove um pacote.
     `_'
          Deleta completamente um pacote: remove o pacote e tamb�m seus
          arquivos de configura��o.

     `i,I'
          Alterna para a exibi��o de informa��es adicionais (na parte de
          baixo da tela).

     `o,O'
          Altera a ordem em que as op��es s�o exibidas.  (na parte de cima
          da tela).

     `v,V'
          Alterna entre o modo de exibi��o resumido e completo.  Quando
          pressionado, voc� verificar� qual o significado das letras EIOM
          que est�o na segunda linha, mas aqui est� um resumo:

               Letra  Significado       	Poss�veis valores
               E      Erro              	Espa�o, R, I
               I      Instalado         	Espa�o, *, -, U, C, I
               O      Previamente marcado	*, -, =, _, n
               M      Marca              	*, -, =, _, n

          (Note que letras em mai�sculo e min�sculo causam efeitos
          absolutamente diferentes)

     Melhor que ficar explicando tudo isso, irei me referir as telas de
     ajuda, onde todos os detalhes ser�o desvendados.  Vamos a um exemplo:

     Voc� entra no `dselect' e v� uma linha como esta:

EIOM Pri  Section  Package   Description
  ** Opt  misc     loadlin   a loader (running under DOS) for LINUX kernel

     Isto significa que o pacote loadlin foi selecionado na �ltima execu��o
     do `dselect' e continua selecionado, mas n�o instalado.  A resposta
     provavelmente � que o pacote loadlin n�o est� fisicamente dispon�vel,
     isto �, n�o consta em seu mirror.

     A informa��o que o `dselect' usa para obter todos os pacotes
     corretamente instalados est� no arquivo Packages (que voc� baixou no
     passo [U]pdate).  Estes arquivos s�o criados pelos pr�prios pacotes.

     Como nada nesse mundo � perfeito, se algo ocorrer e as depend�ncias
     especificadas em um pacote estiverem incorretas, pode ser causada uma
     situa��o que o `dselect' simplesmente n�o pode resolver.  De qualquer
     forma, voc� pode sair deste loop usando os comandos _Q_ e _X_.

     _Q_
          Uma imposi��o.  For�a o `dselect' a ignorar as depend�ncias
          construidas e realizar o voc� especificou.  Isto pode, � claro,
          ser uma p�ssima id�ia.

     _X_
          Use _X_ se voc� estiver totalmente perdido.  Isto coloca as
          coisas novamente em seu devido lugar.

     As teclas que ajudar�o voc� a _n�o_ ficar perdido (!) s�o _R_, _U_ e
     _D_.

     _R_
          Cancela todas as sele��es feitas neste n�vel.  N�o afeta as
          sele��es feitas em um n�vel anterior.

     _U_
          Se o `dselect' propos alguma mudan�a e voc� realizou altera��es
          adicionais, _U_ ir� restaurar as sele��es do `dselect'.

     _D_
          Remove as sele��es feitas pelo `dselect', mantendo apenas as
          suas.

     Segue um exemplo.  O pacote `xmms' (escolhido pois tem muitas
     dependencias) depende destes pacotes:

        * `libc6'

        * `libglib1.2'

        * `libgtk1.2'

        * `xlibs'

     Os seguintes pacotes tamb�m dever�o ser instalados.  No entanto, eles
     n�o s�o essenciais:

        * `libaudiofile0'

        * `libesd0'

        * `libgl1'

        * `libmikmod2'

        * `libogg0'

        * `libvorbis0'

        * `libxml1'

        * `zlib1g'

     Ent�o, quando seleciono `xmms', obtenho uma tela como esta:

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt sound    xmms         Versatile X audio player that looks like Winamp
  _* Opt libs     libglib1.2   The GLib library of C routines
  _* Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  _* Opt libs     libmikmod2   A portable sound library
  _* Opt libs     libogg0      Ogg Bitstream Library
  _* Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     (Outros pacotes podem ou n�o aparecer, dependendo do que est�
     instalado em seu sistema).  Voc� ser� notificado de que todos os
     pacotes requeridos foram selecionados juntamente com alguns
     recomendados.

     A tecla _R_ retorna ao ponto de partida.

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  __ Opt sound    xmms         Versatile X audio player that looks like Winamp
  __ Opt libs     libglib1.2   The GLib library of C routines
  __ Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  __ Opt libs     libmikmod2   A portable sound library
  __ Opt libs     libogg0      Ogg Bitstream Library
  __ Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Para informar agora que voc� n�o quer mais o pacote `xmms', apenas
     pressione _Enter_.

     A tecla _D_retorna para a sele��o que fizemos anteriormente:

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt sound    xmms         Versatile X audio player that looks like Winamp
  __ Opt libs     libglib1.2   The GLib library of C routines
  __ Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  __ Opt libs     libmikmod2   A portable sound library
  __ Opt libs     libogg0      Ogg Bitstream Library
  __ Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     A tecla _U_ restaura a sele��o do `dselect':

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt sound    xmms         Versatile X audio player that looks like Winamp
  _* Opt libs     libglib1.2   The GLib library of C routines
  _* Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  _* Opt libs     libmikmod2   A portable sound library
  _* Opt libs     libogg0      Ogg Bitstream Library
  _* Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Sugiro que, por agora, voc� execute com as sele��s padr�es -- voc�
     ter� grandes oportunidades de adicionar algo mais posteriormente.

     Seja o que for que tenha decidido, pressione _Enter_ para aceitar e
     retornar a tela principal.  Se ocorrerem problemas ainda n�o
     resolvidos, voc� ser� levado novamente para uma tela de corre��o de
     problemas.

     As teclas _R_, _U_ e _D_ s�o muito �teis em diversas situa��es.  Voc�
     poder� testar � vontade e ent�o reinstaurar tudo e reiniciar outra
     vez.  _N�o_ os veja como uma caixa de vidro escrito ``Quebre em caso
     de emerg�ncia.''

     Depois de ter feito suas sele��es na tela ``Select'', pressione _I_
     para obter uma tela maior, _t_ para retornar ao in�cio e ent�o use a
     tecla _Page-Down_ para ver rapidamente as configura��es.  Dessa forma,
     voc� pode checar os resultados de seu trabalho e procurar por erros
     evidentes.  Algumas pessoas t�m desmarcado grupos inteiros de pacotes
     por engano e notam o erro mais tarde demais.  `dselect' � uma
     ferramenta _muito_ poderosa e � bom n�o abusar dela.

     Agora, voc� deve ter a seguinte situa��o:

          package category     status
          
          required             all selected
          important            all selected
          standard             mostly selected
          optional             mostly deselected
          extra                mostly deselected

     Feliz?  Pressione _Enter_ para sair do processo ``Select''.  Voc� pode
     retornar e executar o passo ``Select'' novamente, se desejar.


2.4. ``Install''
----------------

     O `dselect' � executado sobre todos os 8300 pacotes e instala aqueles
     que foram selecionados.  Espere ser questionado para tomar suas
     decis�es.

     A tela rola passando razoavelmente r�pida em uma m�quina r�pida.  Voc�
     pode parar/iniciar isso com _Control-s_/_Control-q_ e no final, voc
     obter� uma lista de pacotes n�o instalados.  Se quiser manter um
     registro de tudo o que acontece, utilize programas comuns do Unix para
     captura��o de sa�da, como tee(1) ou script(1).

     Pode acontecer de um pacote n�o ser instalado por que depende de
     alguns pacotes que est�o listados para instala��o, mas ainda n�o foram
     instalados.  A resposta para isto � executar o passo ``Install''
     novamente.  H� casos em que foi necess�rio executar 4 vezes antes de
     tudo ser instalado.  Isto pode variar conforme o m�todo escolhido;
     como o m�todo APT, voc� provavelmente nunca precisar� executar o
     ``Install'' novamente.


2.5. ``Configure''
------------------

     Muitos pacotes foram configurados no passo 3, mas qualquer coisa que
     passou pode ser configurada aqui.


2.6. ``Remove''
---------------

     Remove pacotes que est�o instalados mas n�o s�o requeridos.


2.7. ``Quit''
-------------

     Sai do `dselect'.

     Eu sugiro que execute `/etc/cron.daily/find' neste ponto, uma vez que
     voc� tem v�rios novos arquivos em seu sistema.  Assim voc� pode usar
     `locate' para obter a localiza��o de qualquer arquivo dado.


-------------------------------------------------------------------------------


3. Algumas Dicas na Conclus�o
-----------------------------

     Voc� pode ter uma id�ia do tamanho de um pacote, pressionando _i_ duas
     vezes e procurando por ``Size''.  Este � o tamanho do pacote
     compactado, ent�o, os arquivos descompactados dever�o ser maiores
     (veja ``Installed-Size'', que est� em kilo-bytes, para saber isso).

     Instalar um novo sistema Debian � uma miss�o um pouco complexa, mas o
     `dselect' poder� auxili�-lo a faz�-lo, se voc� estiver preparado para
     levar o tempo necess�rio para aprender como utiliz�-lo.  Leia as telas
     de ajuda e experimente com _i, I, o,_ e _O_.  Use a tecla _R_.  Est�
     tudo aqui, mas cabe a voc� o seu uso efetivo.


-------------------------------------------------------------------------------


4. Gloss�rio
------------

     Os seguintes termos s�o �teis durante a leitura deste documento e em
     geral, quando falamos sobre a Debian.

     Pacote
          Um arquivo que cont�m tudo o que � necess�rio para instalar e
          executar um programa particular.

          Os nomes dos pacotes Debian t�m o sufixo <.deb>.  Cada pacote tem
          um nome e uma vers�o.  A vers�o consiste na vers�o real
          (`upstream') e a revis�o Debian, separada com um tra�o (`-').

          Aqui est�o alguns exemplos de nomes de pacotes:

             * `efax_08a-1.deb'

             * `lrzsz_0.12b-1.deb'

             * `mgetty_0.99.2-6.deb'

             * `minicom_1.75-1.deb'

             * `term_2.3.5-5.deb'

             * `uucp_1.06.1-2.deb'

             * `uutraf_1.1-1.deb'

             * `xringd_1.10-2.deb'

             * `xtel_3.1-2.deb'

     dpkg
          O programa que manipula os pacotes � o `dpkg'.  O `dselect' � a
          interface para o `dpkg'.  Por ser mais r�pido, usu�rios
          experientes freq�entemente usam o `dpkg' para instalar ou remover
          pacotes.

     Scripts de pacote, scripts do mantenedor
          Os programas (usualmente shell scripts) que o dpkg executa antes
          e depois da instala��o de cada pacote.  Eles s�o silenciosos, mas
          alguns deles podem exibir avisos ou fazer algumas quest�es.


-------------------------------------------------------------------------------


     `dselect' Documenta��o para Iniciantes

     St�phane Bortzmeyer e outros <debian-doc@lists.debian.org>
     Michelle Ribeiro (Tradutora) <michelle@focalinux.org>


