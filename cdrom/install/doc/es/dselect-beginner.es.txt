
              `dselect' Documentaci�n para los principiantes
              ----------------------------------------------

       St�phane Bortzmeyer and others <debian-doc@lists.debian.org>


-------------------------------------------------------------------------------


Resumen
-------

     Este documento contiene un peque�o turial para los usuarios primerizos
     de `dselect', la interfaz de usuario en consola para el manejo de
     paquetes de Debian.  A�ade informaci�n al Manual de Instalaci�n de
     Debian GNU/Linux 3.0
     (http://www.debian.org/releases/woody/i386/install).


-------------------------------------------------------------------------------


Contenidos
----------

     1.        Introducci�n

     2.        Una vez ejecutado `dselect'
     2.1.      �Access�
     2.2.      �Update�
     2.3.      �Select�
     2.4.      �Install�
     2.5.      �Configure�
     2.6.      �Remove�
     2.7.      �Quit�

     3.        Algunos consejos

     4.        Glosario


-------------------------------------------------------------------------------


1. Introducci�n
---------------

     Puede encontrar versiones m�s nuevas de este documento en
     http://www.debian.org/releases/woody/i386/dselect-beginner.

     Este fichero documenta el uso de `dselect' para los usuarios
     primerizos, y su intenci�n es ayudar a lograr una instalaci�n exitosa
     de Debian.  No se intenta explicar todo, por lo que la primera vez que
     se encuentre con `dselect', lea las pantallas de ayuda.

     Si est� impaciente por tener Debian ejecut�ndose lo m�s r�pidamente
     posible, bien, no deber�a usar `dselect' :-) El procedimiento de
     instalaci�n de Debian le permite ejecutar `tasksel', el cual le ofrece
     una serie de tareas generales que usted puede seleccionar.

     `dselect' se usa para seleccionar qu� paquetes desea instalar
     (actualmente existen alrededor de 8300 paquetes en Debian 3.0).  Puede
     ser ejecutado durante la instalci�n, y es algo muy potente y en cierto
     modo complejo, que puede ser usado bien o mal.  Un cierto conocimiento
     previo sobre �l es altamente recomendable.  Usar sin cuidado `dselect'
     puede inutilizar su sistema.

     `dselect' le guiar� a trav�s del proceso de instalaci�n del modo
     siguiente:

        * Elegir el m�todo de acceso a usar.

        * Actualizar la lista de los paquetes disponibles, si es posible.

        * Pedirle qu� paquetes quiere instalar en su sistema.

        * Instalar y actualizar los paquetes deseados.

        * Configurar cualquier paquete a�n no configurado.

        * Borrar el software no deseado.

     Al acabar de forma satisfactoria cada paso, se le dirigir� al
     siguiente.  Rec�rralos en orden, sin saltarse ning�n paso.

     En varias partes de este documento se hablar� de ejecutar otro
     int�rprete de �rdenes.  Linux posee 6 sesiones o int�rpretes de
     �rdenes disponibles en cualquier momento.  Puede intercambiar entre
     ellos pulsando desde _Left Alt-F1_ hasta _Left Alt-F6_, tras lo cual,
     debe registrarse en su sistema.  La consola empleada por el proceso de
     instalaci�n es la primera, tambi�n conocida como tty1, as� que pulse
     _Left Alt-F1_ cuando quiera volver a la instalaci�n.


-------------------------------------------------------------------------------


2. Una vez ejecutado `dselect'
------------------------------

     Una vez que se ejecute `dselect' ver� esta pantalla:

          Debian `dselect' package handling frontend.
          
          0. [A]ccess    Choose the access method to use.
          1. [U]pdate    Update list of available packages, if possible.
          2. [S]elect    Request which packages you want on your system.
          3. [I]nstall   Install and upgrade wanted packages.
          4. [C]onfig    Configure any packages that are unconfigured.
          5. [R]emove    Remove unwanted software.
          6. [Q]uit      Quit dselect.
          
          [algunas cosas m�s]

     En la versi�n localizada aparecer� lo siguiente.  Las opciones son
     totalmente equivalentes en ambos idiomas.

Interfaz de manejo de paquetes `dselect' de Debian.

0. [M]�todo    Escoger el m�todo de acceso que se usar�.
1. [A]ctualiza Actualizar la lista de paquetes disponibles, si se puede.
2. [S]eleccion Solicitar qu� paquetes desea en el sistema.
3. [I]nstalar  Instalar y actualizar los paquetes deseados.
4. [C]onfigura Configurar los paquetes que no est�n configurados.
5. [D]esinstal Desinstalar los paquetes no deseados.
6. sa[L]ir     Salir de dselect.

[algunas cosas m�s]

     Veamos cada una de las opciones.


2.1. �Access�
-------------

     �sta es la pantalla de selecci�n de m�todo:

dselect - lista de m�todos de acceso
  Abbrev.        Descripci�n
  cdrom          Install from a CD-ROM.
* multi_cd       Install from a CD-ROM set.
  nfs            Install from an NFS server (not yet mounted).
  multi_nfs      Install from an NFS server (using the CD-ROM set) (not yet mounted).
  harddisk       Install from a hard disk partition (not yet mounted).
  mounted        Install from a filesystem which is already mounted.
  multi_mount    Install from a mounted partition with changing contents.
  floppy         Install from a pile of floppy disks.
  apt            APT Acquisition [file,http,ftp]

     Aqu� le decimos a `dselect' d�nde se encuantran los paquetes.  Por
     favor, ignore el orden en que �stos aparecen.  Es muy importante que
     seleccione el m�todo de instalacii�n apropiado.  Puede tener m�s o
     menos m�todos listados, o verlos en un orden diferente; simplemente no
     se preocupe por ello.  En la lista siguiente, describimos los
     diferentes m�todos.

     apt
          Una de las mejores opciones para la instalaci�n desde una r�plica
          local del archivo Debian, o desde una red.  Este m�todo usa el
          sistema �apt� (vea apt(8)), para hacer un completo an�lisis de
          las dependencias, por lo que es m�s probable que instale los
          paquetes en el orden �ptimo.

          La configuraci�n de este m�todo es directa.  Puede seleccionar
          cualquier n�mero de localizaciones, mezclando URLs del tipo
          `file:' (discos locales o montados por NFS), URLs del tipo
          `http:', o `ftp:'.  Tambi�n puede incluir medios como CD-ROM o
          DVD con `apt-cdrom'.

          Por favor, consulte la p�gina del manual sources.list(5) para m�s
          informaci�n acerca del formato del fichero
          `/etc/apt/sources.list'.

          Si posee un servidor proxy para HTTP o FTP (o ambos), aseg�rese
          de que establece la variable de entorno `http_proxy' o
          `ftp_proxy', respectivamente.  Establ�zcalas desde su int�rprete
          de �rdenes antes de ejecutar dselect, por ejemplo:

               # export http_proxy=http://gateway:3128/
               # dselect

     multi_cd
          Bastante potente, este m�todo es la forma recomendada de instalar
          una versi�n reciente de Debian desde varios CDs.  Cada unos de
          estos CDs deber�a contener informaci�n acerca de todos los
          paquetes que �l contiene, as� como los que hay en todos los CDs
          previos (en el fichero `Packages.cd').  La primera vez que
          seleccione este m�todo, aseg�rese de que la unidad CD-ROM que
          vaya a usar no est� montada.  Coloque el �ltimo CD _binario_ del
          conjunto (no necesitamos los CDs de fuentes) en la unidad y
          conteste a las preguntas que se le realizan:

             * La localizaci�n de la unidad CD-ROM

             * La confirmaci�n de que est� usando un conjunto de varios CDs

             * La localizaci�n de la distribuci�n Debiann en lo(s) disco(s)

             * [ Posiblemente ] la localizaci�n de otros fichero de
               paquetes (Packages)

          Una vez que haya actualizado la lista de paquetes disponibles y
          haya seleccionado los paquetes que se van a instalar, el m�todo
          multi-cd diverge del procedimiento normal.  Necesitar� realizar
          un paso �Install� para cada uno de los CDs.  Desafortunadamente,
          debido a las limitaciones de dselect, no le pedir� un nuevo disco
          en cada paso; la forma de hacerlo para cada disco es:

             * Inserte el CD en la unidad CD-ROM.

             * Desde el men� principal de dselect, seleccione �Install�.

             * Espere hasta que dpkg termine la instalaci�n de ese CD
               (puede informar de una instalaci�n exitosa, o posibles
               errores en la instalaci�n.  No se preocupe de �stos hasta
               m�s tarde).

             * Pulse [_Enter_] para volver al men� principal de dselect.

             * Repita con el siguiente CD del conjunto...

          Puede ser necesario ejecutar el paso de instalaci�n m�s de una
          vez para lograr la instalaci�n de un paquete - algunos paquetes
          instalados en primer lugar pueden necesitar que se instalen
          paquetes m�s tarde para que puedan ser configurados
          correctamente.

          Recomendamos que ejecute el paso �Configure�, ayudando as� a
          arreglar cualquier paquete que pueda acabar en este estado.

     multi_nfs, multi_mount
          �stos son muy similares al m�todo multi_cd de antes, y son
          simples refinamientos en la cuesti�n de instalar con un medio
          intercambiable, por ejemplo en el caso de instalar desde un
          conjunto de varios CDs exportados por NFS desde la unidad de
          CD-ROM de otra m�quina.

     floppy
          Disponibles para aquellas personas sin CD-ROM o acceso a una red.
          No son recomendables como una opci�n de instalaci�n si est�
          usando disquettes de tama�o tradicional, pero puede funcionar
          mejor si usa dispositivos como LS/120 o Zip.  Especifique la
          localizaci�n de su unidad de disquettes, e introduzca �stos.  El
          primero de ellos deber�a contener el fichero Packages.  Este
          m�todo es lento y poco fiable debido a los problemas del medio.

     nfs
          _M�TODO OBSOLETO -- use el m�todo apt o multi_nfs en su lugar.
          �S�lo intente emplear este m�todo si los otros fallan!_

          Este es un m�todo de instalaci�n simple, con necesidades muy
          simples: introduzca la direcci�n del servidor NFS, la
          localizaci�n de la distribuci�n Debian en el servidor y (quiz�)
          del o de los ficheros �Packages�.  `dselect' instalar� las
          diversas secciones desde el servidor.  Este m�todo, lento pero
          f�cil, no ordena los paquetes a instalar, por lo que requerir�
          varias ejecuciones de los pasos �Install� y/o �Configure�.
          Obviamente, es s�lo apropiado para instalaciones basadas en NFS.

     harddisk
          _M�TODO OBSOLETO -- use el m�todo apt o multi_mount en su lugar.
          �S�lo intente emplear este m�todo si los otros fallan!_

          Especifique la partici�n a usar, y la localizaci�n de los
          ficheros de Debian en esa partici�n, como es usual.  Lento pero
          f�cil.  No ordena la instalaci�n de los paquetes, por lo que
          requerir� varias ejecuciones de los pasos �Install� y/o
          �Configure�.  No es recomendable, pues el m�todo �apt� soporta
          esta funcionalidad, ordenando de forma adecuada los paquetes.

     mounted
          _M�TODO OBSOLETO -- use el m�todo apt o multi_mount en su lugar.
          �S�lo intente emplear este m�todo si los otros fallan!_

          Simplemente especifique la o las localizaciones de los ficheros
          de Debian en su sistema de ficheros.  Posiblemete el m�todo m�s
          sencillo, pero lento.  No ordena la instalaci�n de los paquetes,
          por lo que requerir� varias ejecuciones de los pasos �Install�
          y/o �Configure�.

     cdrom
          _M�TODO OBSOLETO -- use el m�todo multi_cd en su lugar.  Este
          m�todo no funciona con un conjunto m�ltiple de CDs, como los que
          se incluyen en Debian 3.0._

          Dise�ado para instalaciones con un �nico CD, este m�todo sencillo
          le preguntar� por la localizaci�n de su unidad de CD-ROM, la
          localizaci�n de la distribuci�n Debian en ese disco y (si es
          necesario) la localizaci�n de los ficheros �Packages� en el
          disco.  Simple, pero bastante lento.  No ordena la instalaci�n de
          los paquetes, por lo que requerir� varias ejecuciones de los
          pasos �Install� y/o �Configure�.  No es recomendable, pues asume
          que la distribuci�n est� en un �nico CD, lo cual ya no es el
          caso.  Emplee el m�todo �multi_cd� en su lugar.

     Si encuentra cualquier tipo de problemas -- como que Linux no
     encuentra su unidad de CD_ROM, su sistema NFS no funciona o ha
     olvidado en qu� partici�n est�n los paquetes -- tiene un par de
     opciones:

        * Arrancar otro int�rprete de �rdenes, arreglar el problema y
          volver al proceso de instalaci�n.

        * Salir de `dselect' y volverlo a ejecutar m�s tarde.  Puede que
          incluso deba reiniciar el ordenador para resolver algunas
          cuestiones.  Esto no causa problemas, pero cuando vuelva a
          ejecutar `dselect' h�galo como root.  No se ejecutar�
          autom�ticamente despu�s de la primera vez.

     Despu�s de que elija el m�todo de acceso, `dselect' le pedir� que
     indique la localizaci�n exacta de los paquetes.  Si no lo hace
     correctamente la primera vez, pulse _Control-C_ y vuelva a la opci�n
     �Access�.

     Una vez que termine este paso, volver� a la pantalla principal.


2.2. �Update�
-------------

     `dselect' leer� los ficheros `Packages' o `Packages.gz' desde la
     r�plica y crear� una base de datos en su sistema con todos los
     paquetes dispoibles.  Descargar y procesar los ficheros puede llevar
     un tiempo.


2.3. �Select�
-------------

     Abr�chense los cinturoes.  Aqu� es donde ocurre todo.  El prop�sito de
     este paso es seleccionar qu� paquetes desea instalar.

     Pulse _Enter_.  Si est� en un ordenador lento, d�se cuenta de que la
     pantalla se borrar� y puede permanecer en blanco durante unos 15
     segundos.  No empieze a pulsar teclas, sea paciente.

     Lo primero que aparece en la pantalla es la p�gina 1 del fichero de
     Ayuda.  Puede acceder a �l en cualquier momento pulsado _?_ en las
     pantallas del paso �Select� y puede navegar por �l pulsando la tecla
     _._ (punto).

     Por favor, prep�rese para pasar alrededor de una hora para aprendiendo
     c�mo hacer esto de forma correcta.  Cuando aparezca la ventana del
     paso �Select� por primera vez, no haga _NINGUNA_ selecci�n --
     simplemente pulse _Enter_ y vea los problemas de dependencias que
     existen.  Intente arreglarlos.  Si aparece de nuevo en la pantalla
     principal, vuelva a entrar en el paso Secci�n 2.3, `�Select�' de
     nuevo.

     Antes de empezar a bucear, tenga en cuenta que:

        * Para salir de la pantalla de �Select� despu�s de que la selecci�n
          sea la deseada, pulse _Enter_.  Esto le llevar� a la pantalla
          pricipal si no hay ning�n problema con su selecci�n.  En otro
          caso, se le pedir� que resuelva el problema.  Cuando est�
          contento con las pantallas que se le muestran, pulse _Enter_ de
          nuevo para salir.

        * Los problemas son normales y son de esperar.  Si selecciona el
          paquete <A> y ese paquete necesita del paquete <B> para
          funcionar, `dselect' le avisar� del problema y le propondr� una
          soluci�n.  Si el paquete <A> presenta coflictos con el paquete
          <B> (por ejemplo, si son mutuamente excluyentes) se le pedir� que
          decida entre ellos.

     Veamos qu� hay en las dos primeras l�neas de la pantalla de �Select�.

dselect - main package listing (avail., priority)    mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Inst.ver    Avail.ver   Description

     Esta cabecera le recuerda alguna de las teclas especiales:

     `+'
          Selecciona un paquete para ser instalado.

     `='
          Bloquea un paquete -- �til para paquetes rotos.  Puede instalar
          una versi�n anterior y bloquearla mientras espera que aparezca
          una nueva.  (Estas cosas pasan raramente con la rama estable de
          Debian).

     `-' Desinstalar un paquete.
     `_'
          Purgar un paquete: borra el paquete y sus ficheros de
          configuraci�n.

     `i,I'
          Habilitar o rotar la visualizaci�n de informaci�n adicional (en
          la parte baja de la pantalla).

     `o,O'
          Cambia entre los diversos modos de ordenar los paquetes (en la
          parte superior de la pantalla).

     `v,V'
          Cambia entre el modo expresivo y abreviado.  Cuando pulsa esta
          tecla, ver� lo que las letras EIOM significan.  Pero aqu� est� un
          resumen:

               Letra  Significado       Posibles Valores
               E      Error             Espacio, R, I
               I      Estado		 Espacio, *, -, U, C, I
               O      Antiguo		 *, -, =, _, n
               M      Marcado	         *, -, =, _, n

          (Las may�sculas y min�sculas tienen diferente comportamiento.)

     En vez de describir todo aqu�, le recomendamos las pantallas de ayuda,
     donde puede encontrar informaci�n sobre todo esto.  Un ejemplo, sin
     embargo:

     Arranca `dselect' y encuentra una l�nea como esta:

EIOM Pri  Section  Package   Description
  ** Opt  misc     loadlin   a loader (running under DOS) for LINUX kernel

     Esto significa que loadlin fue seleccionado la �ltima vez que ejecut�
     `dselect' y que a�n lo est�, pero que no est� instalado.  �Por qu�?
     La respuesta puede ser que el paquete loadlin no est� disponible, es
     decir, que no se encuentra en su r�plica.

     La informaci�n que `dselect' emplea para obtener todos los paquetes
     que est�n correctamente instalados, est� inclu�da en los ficheros
     �Packages� (que descarg� en el paso �Update�).  Estos ficheros se
     generan a partir de los propios paquetes.

     Nada en este mundo es perfecto, y de vez en cuando sucede que las
     dependencias establecidas en un paquete son incorrectas, lo que puede
     producir una situaci�n que `dselect' no puede resolver.  Sin embargo,
     puede romper el ciclo usando los comando _Q_ y _X_.

     _Q_
          Fuerza a `dselect' a ignorar las dependencias propias del paquete
          y hacer lo que le haya especificado.  Por supuesto, esto podr�a
          convertirse en una mala idea.

     _X_
          Use _X_ si se ha perdido por completo.  Deshace los cambios que
          haya hecho y sale del programa.

     Las teclas que le pueden permitir _no_ perderse son _R_, _U_ y _D_.

     _R_
          Cancela todas las selecciones que haya realizado en este nivel.
          No afecta a las selecciones que haya hecho en el nivel anterior.

     _U_
          Si `dselect' propuso cambios y luego usted a�adi� otros, _U_
          restaurar� la selecci�n inicial hecha por `dselect'.

     _D_
          Borra las selecciones hechas por `dselect', dejando s�lo las que
          ha realizado usted.

     Un ejemplo es el siguiente.  El paquete `xmms' (que hemos elegido pues
     posee un gran n�mero de dependencias) depende de estos paquetes:

        * `libc6'

        * `libglib1.2'

        * `libgtk1.2'

        * `xlibs'

     Los siguientes paqutes ser�n tambi�n instaldos, aunque no son
     esenciales:

        * `libaudiofile0'

        * `libesd0'

        * `libgl1'

        * `libmikmod2'

        * `libogg0'

        * `libvorbis0'

        * `libxml1'

        * `zlib1g'

     Por tanto, cuando seleccionamos el paquete `xmms', obtenemos una
     pantalla como esta:

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt sound    xmms         Versatile X audio player that looks like Winamp
  _* Opt libs     libglib1.2   The GLib library of C routines
  _* Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  _* Opt libs     libmikmod2   A portable sound library
  _* Opt libs     libogg0      Ogg Bitstream Library
  _* Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     (Pueden aparecer otros paquetes o no aparecer algunos de los listados
     aqu�, dependiendo de qu� est� instalado en su sistema).  Se dar�
     cuenta de que todos los paquetes requeridos ya han sido seleccionados
     por nosotros, junto con los recomendados.

     La tecla _R_ hace que todo vuelva al estado inicial.

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  __ Opt sound    xmms         Versatile X audio player that looks like Winamp
  __ Opt libs     libglib1.2   The GLib library of C routines
  __ Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  __ Opt libs     libmikmod2   A portable sound library
  __ Opt libs     libogg0      Ogg Bitstream Library
  __ Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Si decide ahora que no quiere instalar el paquete `xmms', simplemente
     pulse _Enter_.

     La tecla _D_ deja las cosas como aparecieron por primera vez cuando
     seleccionamos el paquete.

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt sound    xmms         Versatile X audio player that looks like Winamp
  __ Opt libs     libglib1.2   The GLib library of C routines
  __ Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  __ Opt libs     libmikmod2   A portable sound library
  __ Opt libs     libogg0      Ogg Bitstream Library
  __ Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     La tecla _U_ restaura las selecciones hechas por `dselect':

dselect - recursive package listing                  mark:+/=/- verbose:v help:?
EIOM Pri Section  Package      Description
  _* Opt sound    xmms         Versatile X audio player that looks like Winamp
  _* Opt libs     libglib1.2   The GLib library of C routines
  _* Opt libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  _* Opt libs     libmikmod2   A portable sound library
  _* Opt libs     libogg0      Ogg Bitstream Library
  _* Opt libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Le recomendamos que por ahora emplee las opciones por defecto --
     tendr� la oportunidad de a�adir otras m�s tarde.

     Decida lo que decida, pulse _Enter_ para aceptar los cambios y volver
     a la pantalla principal.  Si esto causa problemas que a�n no han sido
     resuletos, se le enviar� a una nueva pantalla de resoluci�n de
     problemas.

     Las teclas _R_, _U_, y _D_ son muy �tiles si quiere saber ��Qu�
     pasar�a si...�.  Puede experimentar a voluntad y luego anular todo y
     volver a empezar.  _No_ piense en ellas como en algo que est� detr�s
     de un cristal etiquetado: �Romper en caso de emergencia.�

     Despu�s de realizar sus selecciones en la pantalla de �Select�, pulse
     _I_ para que se le muestre una gran ventana, _t_ para ir al principio
     y luego use la tecla _Av Pag_ para hojear los paquetes que ha
     seleccionado.  De este modo puede comprobar los resultados de su
     trabajo y corregir errores.  Algunas personas han deseleccionado
     grupos importantes de paquetes por error, y no se han dado cuenta
     hasta que ha sido demasiado tarde.  `dselect' es una utilidad _muy_
     potente, as� que es mejor que trate de usarla correctamenete.

     Ahora debiera tener esta situaci�n:

          Categor�a	     Estado
          
          required             todos seleccionados
          important            todos seleccionados
          standard             la mayor parte seleccionados
          optional             la mayor parte no seleccionados
          extra                la mayor parte no seleccionados

     �Feliz?  Pulse _Enter_ para salir del paso �Select�.  Puede volver a
     este paso de nuevo si lo desea.


2.4. �Install�
--------------

     `dselect' recorre todos los 8300 paquetes e instala los seleccionados.
     Espere que se le pregunte acerca de ciertas decisiones durante este
     proceso.

     La pantalla se desplaza bastante vel�zmente en un ordenador r�pido.
     Puede parar o reanudar el proceso con _Control-s_/_Control-q_ y al
     final del mismo obtendr� una lista de los paquetes que no se pudieron
     instalar.  Si desea guardar una copia de todo lo que sucede, emplee
     los programas comunes de Unix para capturar la salida, como tee(1) o
     script(1).

     Puede suceder que un paquete no se instale porque dependa de otro
     paquete que est� seleccionado para instalarse, pero a�n no lo ha
     hecho.  La forma de resolver esto es ejecutar de nuevo el paso
     �Install�.  En algunas ocasiones ha sido necesario ejecutar este paso
     hasta 4 veces antes de que todo quede en su sitio.  Esto var�a con el
     m�todo que emplee para obtener los paquetes; con APT es muy improbable
     que tenga que volver a ejecutarlo.


2.5. �Configure�
----------------

     La mayor parte de los paquetes quedan configurados en el paso 3, pero
     lo que no sea as� se puede configurar mediante este paso.


2.6. �Remove�
-------------

     Borra los paquetes que estaban instalados, pero que no se necesitan
     m�s.


2.7. �Quit�
-----------

     Sale de `dselect'.

     Le sugerimos que ejecute `/etc/cron.daily/find' en este momento, pues
     acaba de isntalar un gran n�mero de nuevos ficheros en su sistema.
     Despu�s de esto, podr� emplear la orden `locate' para buscar ficheros.


-------------------------------------------------------------------------------


3. Algunos consejos
-------------------

     Se puede hacer una idea del tama�o de un paquete pulsando _i_ dos
     veces y mirado el campo �Size�.  �ste es el tama�o del paquete
     comprimido, por lo que los ficheros descomprimidos ocupar�n mucho m�s
     espacio (mire el campo �Installed-Size�, que est� en kilobytes, para
     saber �ste tama�o).

     Instalar un nuevo sistema Debian puede ser una misi�n compleja, pero
     `dselect' le puede ayudar a hacerlo, si se toma el tiempo de aprender
     a `conducirlo'.  Lea las pantallas de ayuda y experimente con _i, I,
     o,_ y _O_.  Use la tecla _R_.  Est� todo ah�, pero depende de usted el
     usarlo de una forma eficaz.


-------------------------------------------------------------------------------


4. Glosario
-----------

     Los siguientes t�rminos son �tiles para usted en este documento y en
     general cuando se hable sobre Debian.

     Paquete
          Un fichero que contiene todo lo necesario para instalar y
          ejecutar un determinado programa.

          Los nombres de los paquetes de Debian poseen el sufijo <.deb>.
          Cada paquete tiene un nombre y una versi�n.  La versi�n consta de
          la versi�n en s� (`upstream'), y de la revisi�n de Debian,
          separadas por un gui�n (`-').

          Aqu� hay alguos ejemplos de nombres de paquetes:

             * `efax_08a-1.deb'

             * `lrzsz_0.12b-1.deb'

             * `mgetty_0.99.2-6.deb'

             * `minicom_1.75-1.deb'

             * `term_2.3.5-5.deb'

             * `uucp_1.06.1-2.deb'

             * `uutraf_1.1-1.deb'

             * `xringd_1.10-2.deb'

             * `xtel_3.1-2.deb'

     dpkg
          El programa que maneja los paquetes es `dpkg'.  `dselect' es una
          interfaz de usuario para `dpkg'.  Los usuarios experimentados en
          ocasiones usan `dpkg' directamente para instalar o borrar un
          paquete, pues es m�s r�pido.

     �scripts� del paquete, �scripts� del mantenedor
          Son los programas (habitualmente �scripts� del int�rprete de
          �rdenes) que dpkg ejecuta antes y despu�s de instalar cada
          paquete.  Habitualmente son silenciosos, pero algunos de ellos
          pueden mostrar avisos o preguntarle cuestiones.


-------------------------------------------------------------------------------


     `dselect' Documentaci�n para los principiantes

     St�phane Bortzmeyer and others <debian-doc@lists.debian.org>


