<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">

<html>

<head>

<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>Installing Debian GNU/Linux 3.0 For Intel x86 - Partitioning for Debian</title>

</head>

<body>

<a name="ch-partitioning"></a>
<hr>

[ <a href="ch-rescue-boot.en.html">previous</a> ]
[ <a href="install.en.html#contents">Contents</a> ]
[ <a href="ch-welcome.en.html">1</a> ]
[ <a href="ch-hardware-req.en.html">2</a> ]
[ <a href="ch-preparing.en.html">3</a> ]
[ <a href="ch-install-methods.en.html">4</a> ]
[ <a href="ch-rescue-boot.en.html">5</a> ]
[ 6 ]
[ <a href="ch-install-system.en.html">7</a> ]
[ <a href="ch-init-config.en.html">8</a> ]
[ <a href="ch-post-install.en.html">9</a> ]
[ <a href="ch-boot-floppy-techinfo.en.html">10</a> ]
[ <a href="ch-appendix.en.html">11</a> ]
[ <a href="ch-administrivia.en.html">12</a> ]
[ <a href="ch-install-system.en.html">next</a> ]

<hr>

<h1>
Installing Debian GNU/Linux 3.0 For Intel x86
<br>Chapter 6 - Partitioning for Debian
</h1>


<hr>


<p>
The ``Partition a Hard Disk'' menu item presents you with a list of disk drives
you can partition, and runs a partitioning application.  You must create at
least one ``Linux native'' (type 83) disk partition, and you probably want at
least one ``Linux swap'' (type 82) partition.

<hr>

<a name="s-partition-intro"></a>
<h2>6.1 Deciding on Debian Partitions and Sizes</h2>

<p>
At a bare minimum, GNU/Linux needs one partition for itself.  You can have a
single partition containing the entire operating system, applications, and your
personal files.  Most people feel that a separate swap partition is also a
necessity, although it's not strictly true.  ``Swap'' is scratch space for an
operating system, which allows the system to use disk storage as ``virtual
memory''.  By putting swap on a separate partition, Linux can make much more
efficient use of it.  It is possible to force Linux to use a regular file as
swap, but it is not recommended.

<p>
Most people choose to give GNU/Linux more than the minimum number of
partitions, however.  There are two reasons you might want to break up the file
system into a number of smaller partitions.  The first is for safety.  If
something happens to corrupt the file system, generally only one partition is
affected.  Thus, you only have to replace (from the backups you've been
carefully keeping) a portion of your system.  At a bare minimum, you should
consider creating what is commonly called a ``root partition''.  This contains
the most essential components of the system.  If any other partitions get
corrupted, you can still boot into GNU/Linux to fix the system.  This can save
you the trouble of having to reinstall the system from scratch.

<p>
The second reason is generally more important in a business setting, but it
really depends on your use of the machine.  Suppose something runs out of
control and starts eating disk space.  If the process causing the problem
happens to have root privileges (the system keeps a percentage of the disk away
from users), you could suddenly find yourself out of disk space.  This is not
good as the OS needs to use real files (besides swap space) for many things.
It may not even be a problem of local origin.  For example, getting spammed
with e-mail can easily fill a partition.  By using more partitions, you protect
the system from many of these problems.  Using mail as an example again, by
putting <code>/var/mail</code> on its own partition, the bulk of the system
will work even if you get spammed.

<p>
The only real drawback to using more partitions is that it is often difficult
to know in advance what your needs will be.  If you make a partition too small
then you will either have to reinstall the system or you will be constantly
moving things around to make room in the undersized partition.  On the other
hand, if you make the partition too big, you will be wasting space that could
be used elsewhere.  Disk space is cheap nowadays, but why throw your money
away?

<hr>

<a name="s-directory-tree"></a>
<h2>6.2 The Directory Tree</h2>

<p>
Debian GNU/Linux adheres to the <code><a
href="http://www.pathname.com/fhs/">Filesystem Hierarchy Standard</a></code>
for directory and file naming.  This standard allows users and software
programs to predict the location of files and directories.  The root level
directory is represented simply by the slash <code>/</code>.  At the root
level, all Debian systems include these directories:

<pre>
            bin       Essential command binaries
            boot      Static files of the boot loader
            dev       Device files
            etc       Host-specific system configuration
            home      User home directories
            lib       Essential shared libraries and kernel modules
            mnt       Mount point for mounting a file system temporarily
            proc      Virtual directory for system information
            root      Home directory for the root user
            sbin      Essential system binaries
            tmp       Temporary files
            usr       Secondary hierarchy
            var       Variable data
            opt       Add-on application software packages
</pre>

<p>
The following is a list of important considerations regarding directories and
partitions.
<ul>
<li>
The root partition <code>/</code> must always physically contain
<code>/etc</code>, <code>/bin</code>, <code>/sbin</code>, <code>/lib</code> and
<code>/dev</code>, otherwise you won't be able to boot.  Typically 100 MB is
needed for the root partition, but this may vary.
</li>
</ul>
<ul>
<li>
<code>/usr</code>: all user programs (<code>/usr/bin</code>), libraries
(<code>/usr/lib</code>), documentation (<code>/usr/share/doc</code>), etc., are
in this directory.  This part of the file system needs most of the space.  You
should provide at least 500 MB of disk space.  If you want to install more
packages you should increase the amount of space you give this directory.
</li>
</ul>
<ul>
<li>
<code>/home</code>: every user will put his data into a subdirectory of this
directory.  The size of this depends on how many users will be using the system
and what files are to be stored in their directories.  Depending on your
planned usage you should reserve about 100 MB for each user, but adapt this
value to your needs.
</li>
</ul>
<ul>
<li>
<code>/var</code>: all variable data like news articles, e-mails, web sites,
APT's cache, etc.  will be placed under this directory.  The size of this
directory depends greatly on the usage of your computer, but for most people
will be dictated by the package management tool's overhead.  If you are going
to do a full installation of just about everything Debian has to offer, all in
one session, setting aside 2 or 3 gigabytes of space for <code>/var</code>
should be sufficient.  If you are going to install in pieces (that is to say,
install services and utilities, followed by text stuff, then X, ...), you can
get away with 300 - 500 megabytes of in <code>/var</code>.  If hard drive space
is at a premium and you don't plan on using APT, at least not for major
updates, you can get by with as little as 30 or 40 megabytes in
<code>/var</code>.
</li>
</ul>
<ul>
<li>
<code>/tmp</code>: if a program creates temporary data it will most likely go
in <code>/tmp</code>.  20-50 MB should be usually enough.
</li>
</ul>

<hr>

<a name="s6.3"></a>
<h2>6.3 PC Disk Limitations</h2>

<p>
The PC BIOS generally adds additional constraints for disk partitioning.  There
is a limit to how many ``primary'' and ``logical'' partitions a drive can
contain.  Additionally, with pre 1994-98 BIOS, there are limits to where on the
drive the BIOS can boot from.  More information can be found in the <code><a
href="http://www.tldp.org/HOWTO/mini/Partition/">Linux Partition
HOWTO</a></code> and the <code><a
href="http://www.phoenix.com/pcuser/BIOS/biosfaq2.htm">Phoenix BIOS
FAQ</a></code>, but this section will include a brief overview to help you plan
most situations.

<p>
``Primary'' partitions are the original partitioning scheme for PC disks.
However, there can only be four of them.  To get past this limitation,
``extended'' and ``logical'' partitions were invented.  By setting one of your
primary partitions as an extended partition, you can subdivide all the space
allocated to that partition into logical partitions.  You can create up to 60
logical partitions per extended partition; however, you can only have one
extended partition per drive.

<p>
Linux limits the partitions per drive to 15 partitions for SCSI disks (3 usable
primary partitions, 12 logical partitions), and 63 partitions on an IDE drive
(3 usable primary partitions, 60 logical partitions).

<p>
If you have a large IDE disk, and are using neither LBA addressing, nor overlay
drivers (sometimes provided by hard disk manufacturers), then the boot
partition (the partition containing your kernel image) must be placed within
the first 1024 cylinders of your hard drive (usually around 524 megabytes,
without BIOS translation).

<p>
This restriction doesn't apply if you have a BIOS newer than around 1995-98
(depending on the manufacturer) that supports the ``Enhanced Disk Drive Support
Specification''.  Both Lilo, the Linux loader, and Debian's alternative
<code>mbr</code> must use the BIOS to read the kernel from the disk into RAM.
If the BIOS int 0x13 large disk access extensions are found to be present, they
will be utilized.  Otherwise, the legacy disk access interface is used as a
fall-back, and it cannot be used to address any location on the disk higher
than the 1023rd cylinder.  Once Linux is booted, no matter what BIOS your
computer has, these restrictions no longer apply, since Linux does not use the
BIOS for disk access.

<p>
If you have a large disk, you might have to use cylinder translation
techniques, which you can set from your BIOS setup program, such as LBA
(Logical Block Addressing) or CHS translation mode (``Large'').  More
information about issues with large disks can be found in the <code><a
href="http://www.tldp.org/HOWTO/Large-Disk-HOWTO.html">Large Disk
HOWTO</a></code>.  If you are using a cylinder translation scheme, and the BIOS
does not support the large disk access extensions, then your boot partition has
to fit within the <em>translated</em> representation of the 1024th cylinder.

<p>
The recommended way of accomplishing this is to create a small (5-10MB should
suffice) partition at the beginning of the disk to be used as the boot
partition, and then create whatever other partitions you wish to have, in the
remaining area.  This boot partition <em>must</em> be mounted on
<code>/boot</code>, since that is the directory where the Linux kernel(s) will
be stored.  This configuration will work on any system, regardless of whether
LBA or large disk CHS translation is used, and regardless of whether your BIOS
supports the large disk access extensions.

<hr>

<a name="s6.4"></a>
<h2>6.4 Recommended Partitioning Scheme</h2>

<p>
For new users, personal Debian boxes, home systems, and other single-user
setups, a single <code>/</code> partition (plus swap) is probably the easiest,
simplest way to go.  It is possible to have problems with this idea, though,
with larger (20GB) disks.  Based on limitations in how ext2 works, avoid any
single partition greater than 6GB or so.

<p>
For multi-user systems, it's best to put <code>/usr</code>, <code>/var</code>,
<code>/tmp</code>, and <code>/home</code> each on their own partitions separate
from the <code>/</code> partition.

<p>
You might need a separate <code>/usr/local</code> partition if you plan to
install many programs that are not part of the Debian distribution.  If your
machine will be a mail server, you might need to make <code>/var/mail</code> a
separate partition.  Often, putting <code>/tmp</code> on its own partition, for
instance 20 to 50MB, is a good idea.  If you are setting up a server with lots
of user accounts, it's generally good to have a separate, large
<code>/home</code> partition.  In general, the partitioning situation varies
from computer to computer depending on its uses.

<p>
For very complex systems, you should see the <code><a
href="http://www.tldp.org/HOWTO/Multi-Disk-HOWTO.html">Multi Disk
HOWTO</a></code>.  This contains in-depth information, mostly of interest to
ISPs and people setting up servers.

<p>
With respect to the issue of swap partition size, there are many views.  One
rule of thumb which works well is to use as much swap as you have system
memory.  It also shouldn't be smaller than 16MB, in most cases.  Of course,
there are exceptions to these rules.  If you are trying to solve 10000
simultaneous equations on a machine with 256MB of memory, you may need a
gigabyte (or more) of swap.

<p>
On 32-bit architectures (i386, m68k, 32-bit SPARC, and PowerPC), the maximum
size of a swap partition is 2GB (on Alpha and SPARC64, it's so large as to be
virtually unlimited).  This should be enough for nearly any installation.
However, if your swap requirements are this high, you should probably try to
spread the swap across different disks (also called ``spindles'') and, if
possible, different SCSI or IDE channels.  The kernel will balance swap usage
between multiple swap partitions, giving better performance.

<p>
As an example, one of the authors' home machine has 32MB of RAM and a 1.7GB IDE
drive on <code>/dev/hda</code>.  There is a 500MB partition for another
operating system on <code>/dev/hda1</code> (should have made it 200MB as it
never gets used).  A 32MB swap partition is used on <code>/dev/hda3</code> and
the rest (about 1.2GB on <code>/dev/hda2</code>) is the Linux partition.

<p>
For more examples, see <code><a
href="http://www.tldp.org/HOWTO/mini/Partition/partition-5.html#SUBMITTED">Partitioning
Strategies</a></code>.  For an idea of the space taken by tasks you might be
interested in adding after your system installation is complete, check <a
href="ch-appendix.en.html#s-tasksel-size-list">Disk Space Needed for Tasks,
Section 11.4</a>.

<hr>

<a name="s-disk-naming"></a>
<h2>6.5 Device Names in Linux</h2>

<p>
Linux disks and partition names may be different from other operating systems.
You need to know the names that Linux uses when you create and mount
partitions.  Here's the basic naming scheme:
<ul>
<li>
The first floppy drive is named ``/dev/fd0''.
</li>
</ul>
<ul>
<li>
The second floppy drive is named ``/dev/fd1''.
</li>
</ul>
<ul>
<li>
The first SCSI disk (SCSI ID address-wise) is named ``/dev/sda''.
</li>
</ul>
<ul>
<li>
The second SCSI disk (address-wise) is named ``/dev/sdb'', and so on.
</li>
</ul>
<ul>
<li>
The first SCSI CD-ROM is named ``/dev/scd0'', also known as ``/dev/sr0''.
</li>
</ul>
<ul>
<li>
The master disk on IDE primary controller is named ``/dev/hda''.
</li>
</ul>
<ul>
<li>
The slave disk on IDE primary controller is named ``/dev/hdb''.
</li>
</ul>
<ul>
<li>
The master and slave disks of the secondary controller can be called
``/dev/hdc'' and ``/dev/hdd'', respectively.  Newer IDE controllers can
actually have two channels, effectively acting like two controllers.
</li>
</ul>
<ul>
<li>
The first XT disk is named ``/dev/xda''.
</li>
</ul>
<ul>
<li>
The second XT disk is named ``/dev/xdb''.
</li>
</ul>

<p>
The partitions on each disk are represented by appending a decimal number to
the disk name: ``sda1'' and ``sda2'' represent the first and second partitions
of the first SCSI disk drive in your system.

<p>
Here is a real-life example.  Let's assume you have a system with 2 SCSI disks,
one at SCSI address 2 and the other at SCSI address 4.  The first disk (at
address 2) is then named ``sda'', and the second ``sdb''.  If the ``sda'' drive
has 3 partitions on it, these will be named ``sda1'', ``sda2'', and ``sda3''.
The same applies to the ``sdb'' disk and its partitions.

<p>
Note that if you have two SCSI host bus adapters (i.e., controllers), the order
of the drives can get confusing.  The best solution in this case is to watch
the boot messages, assuming you know the drive models and/or capacities.

<p>
Linux represents the primary partitions as the drive name, plus the numbers 1
through 4.  For example, the first primary partition on the first IDE drive is
<code>/dev/hda1</code>.  The logical partitions are numbered starting at 5, so
the first logical partition on that same drive is <code>/dev/hda5</code>.
Remember that the extended partition, that is, the primary partition holding
the logical partitions, is not usable by itself.  This applies to SCSI disks as
well as IDE disks.

<hr>

<a name="s-partition-programs"></a>
<h2>6.6 Debian Partitioning Programs</h2>

<p>
Several varieties of partitioning programs have been adapted by Debian
developers to work on various types of hard disks and computer architectures.
Following is a list of the program(s) applicable for your architecture.
<dl>
<dt><code>fdisk</code></dt>
<dd>
The original Linux disk partitioner, good for gurus; read the <code><a
href="fdisk.txt">fdisk manual page</a></code>.

<p>
Be careful if you have existing FreeBSD partitions on your machine.  The
installation kernels include support for these partitions, but the way that
<code>fdisk</code> represents them (or not) can make the device names differ.
See the <code><a
href="http://www.tldp.org/HOWTO/mini/Linux+FreeBSD-2.html">Linux+FreeBSD
HOWTO</a></code>.
</dd>
</dl>
<dl>
<dt><code>cfdisk</code></dt>
<dd>
A simple-to-use, full-screen disk partitioner for the rest of us; read the
<code><a href="cfdisk.txt">cfdisk manual page</a></code>.

<p>
Note that <code>cfdisk</code> doesn't understand FreeBSD partitions at all,
and, again, device names may differ as a result.
</dd>
</dl>

<p>
One of these programs will be run by default when you select ``Partition a Hard
Disk''.  If the one which is run by default isn't the one you want, quit the
partitioner, go to the shell (<samp>tty2</samp>) by pressing <samp>Alt</samp>
and <samp>F2</samp> keys together, and manually type in the name of the program
you want to use (and arguments, if any).  Then skip the ``Partition a Hard
Disk'' step in <code>dbootstrap</code> and continue to the next step.

<p>
Remember to mark your boot partition as ``Bootable''.

<hr>

<a name="s6.7"></a>
<h2>6.7 ``Initialize and Activate a Swap Partition''</h2>

<p>
This will be the next step once you have created disk partitions.  You have the
choice of initializing and activating a new swap partition, activating a
previously-initialized one, or doing without a swap partition.  It's always
permissible to re-initialize a swap partition, so select ``Initialize and
Activate a Swap Partition'' unless you are sure you know what you are doing.

<p>
This menu choice will first present you with a dialog box reading ``Please
select the partition to activate as a swap device.''.  The default device
presented should be the swap partition you've already set up; if so, just press
<em>Enter</em>.

<p>
Next, there is a confirmation message, since initialization destroys any data
previously on the partition.  If all is well, select ``Yes''.  The screen will
flash as the initialization program runs.

<p>
A swap partition is strongly recommended, but you can do without one if you
insist, and if your system has more than 12MB RAM.  If you wish to do this,
please select the ``Do Without a Swap Partition'' item from the menu.

<hr>

<a name="s-init-partition"></a>
<h2>6.8 ``Initialize a Linux Partition''</h2>

<p>
At this point, the next menu item presented should be ``Initialize a Linux
Partition''.  If it isn't, it is because you haven't completed the disk
partitioning process, or you haven't made one of the menu choices dealing with
your swap partition.

<p>
You can initialize a Linux partition, or alternately you can mount a
previously-initialized one.  Note that <code>dbootstrap</code> will
<em>not</em> upgrade an old system without destroying it.  If you're upgrading,
Debian can usually upgrade itself, and you won't need to use
<code>dbootstrap</code>.  For help on upgrading to Debian 3.0, see the <code><a
href="http://www.debian.org/releases/woody/i386/release-notes/">upgrade
instructions</a></code>.

<p>
Thus, if you are using old disk partitions that are not empty, i.e., if you
want to just throw away what is on them, you should initialize them (which
erases all files).  Moreover, you must initialize any partitions that you
created in the disk partitioning step.  About the only reason to mount a
partition without initializing it at this point would be to mount a partition
upon which you have already performed some part of the installation process
using this same set of installation floppies.

<p>
Select ``Initialize a Linux Partition'' to initialize and mount the
<code>/</code> disk partition.  The first partition that you mount or
initialize will be the one mounted as <code>/</code> (pronounced ``root'').

<p>
You will be asked whether to preserve ``Pre-2.2 Linux Kernel Compatibility?''.
Saying ``No'' here means that you cannot run 2.0 or earlier Linux kernels on
your system, since the file systems enable some features not supported in the
2.0 kernel.  If you know you'll never need to run a 2.0 or earlier vintage
kernel, then you can achieve some minor benefits by saying ``No'' here.

<p>
You will also be asked about whether to scan for bad blocks.  The default here
is to skip the bad block scan, since the scan can be time consuming, and modern
disk drive controllers internally detect and deal with bad blocks.  However, if
you are at all unsure about the quality of your disk drive, or if you have a
rather old system, you should probably do the bad block scan.

<p>
The next prompts are just confirmation steps.  You will be asked to confirm
your action, since initializing is destructive to any data on the partition,
and you will be informed that the partition is being mounted as <code>/</code>,
the root partition.[<a href="footnotes.en.html#f4" name="fr4">4</a>]

<p>
Once you've mounted the <code>/</code> partition, if you have additional file
systems that you wish to initialize and mount, you should use the ``Alternate''
menu item.  This is for those who have created separate partitions for
<code>/boot</code>, <code>/var</code>, <code>/usr</code> or others, which ought
to be initialized and mounted at this time.

<hr>

<a name="s-mount-already-inited"></a>
<h2>6.9 ``Mount a Previously-Initialized Partition''</h2>

<p>
An alternative to <a
href="ch-partitioning.en.html#s-init-partition">``Initialize a Linux
Partition'', Section 6.8</a> is the ``Mount a Previously-Initialized
Partition'' step.  Use this if you are resuming an installation that was broken
off, or if you want to mount partitions that have already been initialized or
have data on it which you wish to preserve.

<p>
If you are installing a diskless workstation, at this point, you want to NFS
mount your root partition from the remote NFS server.  Specify the path to the
NFS server in standard NFS syntax, namely,

<pre>
     <var>server-name-or-IP</var>:<var>server-share-path</var>
</pre>

<p>
.  If you need to mount additional file systems as well, you can do that at
this time.

<p>
If you have not already setup your network as described in <a
href="ch-install-system.en.html#s-configure-network">``Configure the Network'',
Section 7.7</a>, then selecting an NFS install will prompt you to do so.

<hr>

<a name="s-mount-other"></a>
<h2>6.10 Mounting Partitions Not Supported by <code>dbootstrap</code></h2>

<p>
In some special situations, <code>dbootstrap</code> might not know how to mount
your file systems (whether root or otherwise).  It may be possible, if you're
an experienced GNU/Linux user, to simply go to <samp>tty2</samp> by pressing
<samp>Alt</samp> and <samp>F2</samp> keys together, and manually run the
commands you need to run in order to mount the partition in question.

<p>
If you are mounting a root partition for your new system, just mount it to
<code>/target</code>, the go back to dbootstrap and continue (perhaps running
the ``View the Partition Table'' step to cause <code>dbootstrap</code> to
re-compute where it is in the installation process.

<p>
For non-root partitions, you'll have to remember to manually modify your new
<code>fstab</code> file so that when you reboot the partition will be mounted.
Wait for that file (<code>/target/etc/fstab</code>) to be written by
<code>dbootstrap</code>, of course, before editing it.

<hr>

[ <a href="ch-rescue-boot.en.html">previous</a> ]
[ <a href="install.en.html#contents">Contents</a> ]
[ <a href="ch-welcome.en.html">1</a> ]
[ <a href="ch-hardware-req.en.html">2</a> ]
[ <a href="ch-preparing.en.html">3</a> ]
[ <a href="ch-install-methods.en.html">4</a> ]
[ <a href="ch-rescue-boot.en.html">5</a> ]
[ 6 ]
[ <a href="ch-install-system.en.html">7</a> ]
[ <a href="ch-init-config.en.html">8</a> ]
[ <a href="ch-post-install.en.html">9</a> ]
[ <a href="ch-boot-floppy-techinfo.en.html">10</a> ]
[ <a href="ch-appendix.en.html">11</a> ]
[ <a href="ch-administrivia.en.html">12</a> ]
[ <a href="ch-install-system.en.html">next</a> ]

<hr>

<p>
Installing Debian GNU/Linux 3.0 For Intel x86

<address>
version 3.0.23, 15 May, 2002<br>
Bruce Perens<br>
Sven Rudolph<br>
Igor Grobman<br>
James Treacy<br>
Adam Di Carlo
</address>

<hr>

</body>

</html>

