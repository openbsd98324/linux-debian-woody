@ echo off
rem Flush any write-cached disk blocks before we leave DOS. 
If your system does not use smartdrv an error message will appear, 
you can safely ignore that.
smartdrv /c

echo Please choose a linux kernel now
echo 1= IDEPCI kernel                 (2.2)
echo 2= compact (SCSI) kernel         (2.2)
echo 3= 2.4.x kernel                  (2.4)
echo 4= vanilla (standard) kernel     (2.2)

choice /c:1234
if errorlevel 4 goto FOUR
if errorlevel 3 goto THREE
if errorlevel 2 goto TWO
if errorlevel 1 goto ONE

echo doing default somehow and going to ONE
goto ONE

:ONE
echo using IDEPCI kernel
loadlin.exe linpci root=/dev/ram ro initrd=root.bin ramdisk_size=16384 disksize=1.44 flavor=idepci

:TWO
echo using compact (SCSI) kernel
loadlin.exe lincompt root=/dev/ram ro initrd=compact.bin ramdisk_size=16384 disksize=1.44 flavor=compact

:THREE
echo using 2.4.x kernel
loadlin.exe lin24 root=/dev/ram ro initrd=bf24.bin ramdisk_size=16384 disksize=1.44 flavor=bf2.4

:FOUR
echo using standard (vanilla) kernel
loadlin.exe linux root=/dev/ram ro initrd=root.bin ramdisk_size=16384 disksize=1.44 TERM=vt102

