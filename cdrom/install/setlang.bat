@ECHO OFF
IF "%1"=="" GOTO eparam
IF NOT EXIST ..\lang\%1\debian.txt GOTO edir
ECHO Insert the rescue floppy in drive A:
PAUSE
COPY ..\lang\%1\*.* A:\ /Y
IF x%1 == xpt COPY ..\lang\fonts\lat1u-16.psf A:\font.psf
IF x%1 == xda COPY ..\lang\fonts\lat1u-16.psf A:\font.psf
IF x%1 == xes COPY ..\lang\fonts\lat1u-16.psf A:\font.psf
IF x%1 == xcs COPY ..\lang\fonts\iso02g.psf A:\font.psf
IF x%1 == xhr COPY ..\lang\fonts\iso02g.psf A:\font.psf
IF x%1 == xhu COPY ..\lang\fonts\iso02g.psf A:\font.psf
IF x%1 == xpl COPY ..\lang\fonts\iso02g.psf A:\font.psf
IF x%1 == xsk COPY ..\lang\fonts\iso02g.psf A:\font.psf
IF x%1 == xeo COPY ..\lang\fonts\iso03g.psf A:\font.psf
IF x%1 == xru COPY ..\lang\fonts\koi8-r.psf A:\font.psf
GOTO end

:eparam
ECHO ERROR: usage: setlang [lang]
ECHO [lang] can be one of ca cs da de eo es fi fr gl hr hu it ja ko pl pt ru sk sv tr
ECHO Remember to use lower case for language name!
GOTO end

:edir
ECHO ERROR: Directory %1 does not exist

:end
