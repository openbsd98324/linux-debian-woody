
                                            (HTML version in README.html)

        Debian GNU/Linux 3.0 r6 "Woody" - Official i386 Binary-1 CD   



   CONTENTS:

     * Intro
     * About This CD
     * Installing
     * Upgrading
     * Last-Minute Notes
     * Using CDs with Dselect and Apt
     * Non-free/Non-US
     * CD Manufacturers
     * More Information


                     Welcome to the exciting world of 
                             Debian GNU/Linux! 

Intro
=====

   This is one of the CD-ROMs of the Debian GNU/Linux distribution.
   Debian is a very extensive collection of software. But it is more. It
   is a complete Operating System (OS) for your computer. And it is free
   (as in "freedom").

   An operating system is the set of basic programs and utilities that
   make your computer run. At the core of an operating system is the
   kernel. The kernel is the most fundamental program on the computer,
   which does all the basic housekeeping and lets you start other
   programs. Debian is kernel independent. It currently uses the Linux
   kernel but work is in progress to provide Debian for other kernels,
   using the Hurd. Most of the basic operating system tools come from the
   GNU project; hence the name GNU/Linux.

   Debian is available for various kinds of computers ("architectures"),
   like "IBM-compatible" PCs (i386), Compaq's Alpha, Motorola's 680x0
   (m68k), Sun's Sparc, Motorola/IBM's PowerPC, and (Strong)ARM
   processors. Check the ports page (http://www.debian.org/ports) for
   more information.

   Read more at

     http://www.debian.org/intro/about


About This CD
=============

   This CD-ROM is labeled

     Debian GNU/Linux 3.0 r6 "Woody" - Official i386 Binary-1

   which means that this CD is number 1 of totally 7 CDs containing
   programs ("binaries") for `i386' computers.

   The programs on the Binary CDs are ordered by popularity. The Binary-1
   CD contains the most popular programs and the installation tools; it
   is possible to install and run Debian with only the Binary-1 CD. The
   other CDs, up to Binary-7, contain mostly special-interest programs.


Installing
==========

   Because Debian is a complete Operating System, the installation
   procedure may seem a bit unusual. You can install Debian GNU/Linux
   either next to your current OS, or as only OS on your computer.

   Read the Installation Manual, on this CD at

     /install/doc/install.en.html     (English version).

   Translations of the Installation Manual, and plain-text and PDF
   versions, are also available in the

     /install/doc

   directory, along with various other documents.

   Programs and other files that are needed for the installation can be
   found on this CD under

     /dists/woody/main/disks-i386

   For the impatient ones: you can start the installation program easily
   by booting off this CD-ROM. Note that not all (esp. older) systems
   support this.

   You can also examine the

     /install

   directory; you might be able to start the installation system directly
   from there.


Upgrading
=========

   If you are already running an older version of the Debian GNU/Linux
   Operating System, you may want to upgrade to version 3.0.

   Upgrade instructions can be found in the Release Notes, available
   online at:

     http://www.debian.org/releases/woody/releasenotes


Last-Minute Notes
=================

   Sometimes, a slight problem with a Debian CD may be discovered well
   after it has been released. If there are any such issues, they will be
   discussed in the Last Minute Notes section of the Debian CD webpage at
   http://www.debian.org/CD.


Using CDs with Dselect and Apt
==============================

   After installing or upgrading, Debian's packaging system can use
   CD-ROMs, local collections, or networked servers (FTP, HTTP) to
   automatically install software from (.deb packages). This is done
   preferably with the `apt' and `dselect' programs. If you do not
   already know how to setup these, here is a brief explanation.

   First of all, comment out every line of /etc/apt/sources.list by
   placing a hash sign (`#') in front of it (if it isn't already there).

   Then you need to add new "deb" lines describing the location(s) where
   apt will get the new packages from. You can add "deb" lines for a
   packages mirror on a local harddisk, and/or FTP/HTTP mirrors, and/or
   (a set of) CD-ROMs. `Apt' will consider all packages that can be found
   via any "deb" line, and use the one with the highest version number,
   giving priority to the first mentioned lines. (So, in case of multiple
   mirror locations, you'd typically first name a local harddisk, then
   CD-ROMs, and finally FTP/HTTP mirrors.)

     * For an archive on a local harddisk, that for example has these
       directories,

         /var/ftp/debian/dists/woody/main/binary-i386/...
         /var/ftp/debian/dists/woody/contrib/binary-i386/...
         ---------------       -----     |\
               \________          \_     | \___
       add:             \           \    |     \
                  --------------- -----  |      \
         deb file:/var/ftp/debian woody main contrib

       As shown, the `dists' is added implicitly, and the 3rd and
       following arguments are used to expand the path into multiple
       directories.

       If you have a complete mirror that also has non-free and non-US,
       you typically have to add lines like these:

         deb file:/var/ftp/debian woody main contrib non-free
         deb file:/var/ftp/debian-non-US woody/non-US main contrib
       non-free

       If you don't have such a structure, other description lines are
       possible, see the sources.list manpage.

     * FTP and HTTP packages mirror addresses can be found on this CD in
       README.mirrors.txt, or on http://www.debian.org/distrib/ftplist,
       look at the "Full list of mirrors".

       If the mirror has these directories:

         ftp://some-server/debian/dists/woody/main/binary-i386/...
         ftp://some-server/debian/dists/woody/contrib/binary-i386/...
         ------------------------       ----- |   |
                     \___                 /   |   |
       add:              \               /    |   |
             ------------------------ -----   |   |
         deb ftp://some-server/debian woody main contrib

       As shown, the `dists' is added implicitly, and the 3rd and
       following arguments are used to expand the path into multiple
       directories.

       If you use a complete mirror that also has non-free and non-US,
       you typically have to add lines like these:

         deb ftp://some-server/debian woody main contrib non-free
         deb ftp://some-server/debian-non-US woody/non-US main contrib
       non-free

       If the mirror doesn't have such a structure, other description
       lines are possible, see the sources.list manpage.

     * If you use (one or more) CD-ROMs, do _not_ add any lines to
       sources.list manually.

       Instead, first make sure there is a line in /etc/fstab that
       connects the /cdrom mount point with your CD-ROM drive (the exact
       `/cdrom' mount point is "required" for apt). For example, if
       /dev/hdc is your CD-ROM drive, /etc/fstab should contain a line

         /dev/hdc      /cdrom     auto    defaults,noauto,ro     0   0

       To test this, insert a CD-ROM and give commands

         mount /cdrom
         ls -alF /cdrom     (should show the CD-ROM's root directory)
         umount /cdrom

       Then, use the command

         apt-cdrom add

       to add the data of each CD-ROM to apt's internal database. This
       command will ask for one CD-ROM per run, and mount/unmount the
       CD-ROM automatically. You should repeat this command for all 3.0
       `Binary' CD-ROMs you have.

   When the sources.list is correct, the final step of the preparation is
   to update apt's internal database with the command

     apt-get update

   Once this is done, you can install packages from the commandline using
   apt-get. For example, if you want to install the packages `commprog'
   and `maxgame', you can give the command

     apt-get install commprog maxgame

   Note that you don't have to enter the complete path, or the `.deb'
   extension. `Apt' will figure this out itself.

   `Dselect' is also able to use `apt' to fetch the packages. To set up
   `dselect', choose [A]ccess in the main menu, and then select `apt'.
   The sources.list file should be okay already, so don't let the program
   make a new one. Then, after an [U]pdate, the configuration is ready.
   You can just keep using [S]elect and [I]nstall to install packages.

   See dselect's online help screens for more information (use the `?'
   key).


Non-free and Non-US
===================

   The package sections `non-free' and `non-US' contain packages that
   either have a restrictive license, or can't be used in some countries
   like the US because of patent problems. It may well be that this CD
   does not contain (all of) these packages. You can get them from our
   FTP/HTTP mirror sites.

   `Non-free' packages are on our "regular" mirrors, listed in
   README.mirrors.html on this CD, or on
   http://www.debian.org/distrib/ftplist ("Full list of mirrors").

   `Non-US' packages are on special mirrors outside the US, listed in
   README.non-US on this CD, or on
   http://www.debian.org/misc/README.non-US.


CD Manufacturers
================

   You are completely free to manufacture and (re-)distribute CD-ROMs of
   the Debian GNU/Linux Operating System, like this one. There is no
   charge from us (but of course donations are always welcome).

   For all needed information, and contact addresses, please refer to

     http://www.debian.org/CD


More Information
================

   There is much more information present on this CD. Besides the already
   mentioned installation procedure, this is the most interesting:

     * /doc/FAQ                        Debian FAQ
     * /doc/constitution.txt           The Debian Constitution
     * /doc/debian-manifesto           The Debian Manifesto
     * /doc/social-contract.txt        Debian's Social Contract
     * /doc/bug-reporting.txt          Bug reporting instructions

   Also on the Internet are many resources. To name a few:

     * http://www.debian.org           The Debian homepage
     * http://www.debian.org/doc       Debian Documentation
     * http://www.debian.org/support   Debian User Support
     * http://www.tldp.org             The Linux Documentation Project
     * http://www.linux.org            General Linux homepage



      See the Debian contact page (http://www.debian.org/contact) for
                       information on contacting us.

                Last Modified: Wed Apr 18 19:09:34 CEST 2001
