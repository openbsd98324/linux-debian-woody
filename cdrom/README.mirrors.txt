                 Debian worldwide mirror sites
                        -----------------------------

Debian is distributed (mirrored) on hundreds of
servers on the Internet. Using a nearby server will probably speed up your
download, and also reduce the load on our central servers and on the
Internet as a whole.

Debian mirrors can be primary and secondary. The definitions are as follows:

  A primary mirror site has good bandwidth, is available 24 hours a day,
  and has an easy to remember name of the form ftp.<country>.debian.org.
  Additionally, most of them are updated automatically after updates to the
  Debian archive. The Debian archive on those sites is normally available
  using both FTP and HTTP protocols.

  A secondary mirror site may have restrictions on what they mirror (due to
  space restrictions). Just because a site is secondary doesn't necessarily
  mean it'll be any slower or less up to date than a primary site.

Use the site closest to you for the fastest downloads possible whether it is
a primary or secondary site. The program `netselect' can be used to
determine the site with the least latency; use a download program such as
`wget' or `rsync' for determining the site with the most throughput.
Note that geographic proximity often isn't the most important factor for
determining which machine will serve you best.

The authoritative copy of the following list can always be found at:
                      http://www.debian.org/mirror/list
If you know of any mirrors that are missing from this list,
please have the site maintainer fill out the form at:
                     http://www.debian.org/mirror/submit
Everything else you want to know about Debian mirrors:
                        http://www.debian.org/mirror/


                         Primary Debian mirror sites
                         ---------------------------

 Country         Site                  Debian archive  Debian non-US archive
 ---------------------------------------------------------------------------
 Austria         ftp.at.debian.org     /debian/        /debian-non-US/
 Australia       ftp.wa.au.debian.org  /debian/        /debian-non-US/
 Australia       ftp.au.debian.org     /debian/        /debian-non-US/
 Bulgaria        ftp.bg.debian.org     /debian/        /debian-non-US/
 Brazil          ftp.br.debian.org     /debian/        /debian-non-US/
 Chile           ftp.cl.debian.org     /debian/        /debian-non-US/
 Czech Republic  ftp.cz.debian.org     /debian/        /debian-non-US/
 Germany         ftp.de.debian.org     /debian/        /debian-non-US/
 Germany         ftp2.de.debian.org    /debian/        /debian-non-US/
 Denmark         ftp.dk.debian.org     /debian/        /debian-non-US/
 Estonia         ftp.ee.debian.org     /debian/        /debian-non-US/
 Spain           ftp.es.debian.org     /debian/        /debian-non-US/
 Finland         ftp.fi.debian.org     /debian/        /debian-non-US/
 France          ftp2.fr.debian.org    /debian/        /debian-non-US/
 France          ftp.fr.debian.org     /debian/        /debian-non-US/
 Great Britain   ftp.uk.debian.org     /debian/        /debian-non-US/
 Hong Kong       ftp.hk.debian.org     /debian/        /debian-non-US/
 Croatia         ftp.hr.debian.org     /debian/        /debian-non-US/
 Hungary         ftp.hu.debian.org     /debian/        /debian-non-US/
 Ireland         ftp.ie.debian.org     /debian/        /debian-non-US/
 Iceland         ftp.is.debian.org     /debian/        /debian-non-US/
 Italy           ftp.it.debian.org     /debian/        /debian-non-US/
 Italy           ftp2.it.debian.org    /debian/        /debian-non-US/
 Japan           ftp.jp.debian.org     /debian/        /debian-non-US/
 Japan           ftp2.jp.debian.org    /debian/        /debian-non-US/
 Korea           ftp.kr.debian.org     /debian/        /debian-non-US/
 Netherlands     ftp.nl.debian.org     /debian/        /debian-non-US/
 Norway          ftp.no.debian.org     /debian/        /debian-non-US/
 New Zealand     ftp.nz.debian.org     /debian/        /debian-non-US/
 Poland          ftp.pl.debian.org     /debian/        /debian-non-US/
 Romania         ftp.ro.debian.org     /debian/        /debian-non-US/
 Russia          ftp.ru.debian.org     /debian/        /debian-non-US/
 Sweden          ftp.se.debian.org     /debian/        /debian-non-US/
 Slovenia        ftp.si.debian.org     /debian/        /debian-non-US/
 Slovakia        ftp.sk.debian.org     /debian/        /debian-non-US/
 Turkey          ftp.tr.debian.org     /debian/        /debian-non-US/
 United States   ftp.us.debian.org     /debian/        Not mirrored.


                   Secondary mirrors of the Debian archive
                   ---------------------------------------

HOST NAME                              FTP                                     HTTP                                      ARCHITECTURES
---------                              ---                                     ----                                      -------------

AR Argentina
------------
debian.logiclinux.com                                                         /debian/                                 i386

AT Austria
----------
ftp.at.debian.org                      /debian/                               /debian/                                 all
gd.tuwien.ac.at                        /opsys/linux/debian/                   /opsys/linux/debian/                     all
debian.mur.at                          /debian/                               /debian/                                 alpha i386 powerpc
ftp.tu-graz.ac.at                      /mirror/debian/                        /mirror/debian/                          all
ftp.univie.ac.at                       /systems/linux/debian/debian/          /systems/linux/debian/debian/            i386
debian.inode.at                        /debian/                               /debian/                                 all

AU Australia
------------
ftp.wa.au.debian.org                   /debian/                               /debian/                                 all
ftp.au.debian.org                      /debian/                               /debian/                                 all
mirror.aarnet.edu.au                   /debian/                               /debian/                                 all
ftp.monash.edu.au                      /pub/linux/debian/                     /pub/linux/debian/                       i386
ftp.uwa.edu.au                         /mirrors/linux/debian/                                                          all
mirror.eftel.com                       /debian/                               /debian/                                 i386
mirror.pacific.net.au                  /debian/                               /debian/                                 all
ftp.iinet.net.au                       /debian/debian/                        /debian/debian/                          all
debian.goldweb.com.au                                                         /debian/                                 i386
mirror.datafast.net.au                 /linux/debian/                         /linux/debian/                           i386
debian.ihug.com.au                     /debian/                               /debian/                                 i386
debian.vicnet.net.au                   /debian/                               /debian/                                 i386

BE Belgium
----------
ftp.tiscali.be                         /debian/                               /debian/                                 all
ftp.kulnet.kuleuven.ac.be              /debian/                               /debian/                                 all
ftp.easynet.be                         /debian/                               /ftp/debian/                             all
ftp.belnet.be                          /debian/                               /debian/                                 all
ftp.debian.skynet.be                   /debian/                               /ftp/debian/                             all
ftp.scarlet.be                         /pub/debian/                                                                    all

BG Bulgaria
-----------
ftp.bg.debian.org                      /debian/                               /debian/                                 alpha arm i386 ia64 m68k mips mipsel powerpc sparc
debian.ludost.net                      /debian/                               /debian/                                 i386
ftp.uni-sofia.bg                       /debian/                                                                        all

BR Brazil
---------
ftp.br.debian.org                      /debian/                               /debian/                                 all
debian.das.ufsc.br                     /pub/debian/                                                                    i386
download.unesp.br                                                             /linux/debian/                           all
ftp.matrix.net.br                      /pub/debian/                           /pub/debian/                             all
sft.if.usp.br                                                                 /debian/                                 i386
linorg.usp.br                          /debian/                               /debian/                                 all
ftp.pop-ce.rnp.br                      /debian/                               /debian/                                 i386
linux.iq.usp.br                                                               /debian/                                 alpha i386 m68k powerpc sparc
debian.fapeal.br                       /debian/                               /debian/                                 all
ftp.pucpr.br                           /debian/                                                                        i386
www.las.ic.unicamp.br                  /pub/debian/                           /pub/debian/                             i386 sparc

BY Belarus
----------
linux.org.by                           /debian/                               /debian/                                 i386

CA Canada
---------
mirror.direct.ca                       /pub/linux/debian/                     /linux/debian/                           i386 ia64 sparc
debian.yorku.ca                                                               /debian/                                 alpha i386 powerpc
debian.pipcom.com                      /debian/debian/                                                                 all
debian.kida.net                                                               /debian.mirror/debian/                   all
ftp3.nrc.ca                            /debian/                               /debian/                                 all
less.cogeco.net                        /pub/debian/                                                                    all
gulus.usherbrooke.ca                   /debian/                               /debian/                                 all
mirror.cpsc.ucalgary.ca                /debian/                               /debian/                                 all
mirror.peer1.net                                                              /debian/                                 all
debian.savoirfairelinux.net            /debian/                               /debian/                                 all
debian.mirror.cygnal.ca                /debian/                               /debian/                                 all

CH Switzerland
--------------
mirror.switch.ch                       /mirror/debian/                        /ftp/mirror/debian/                      all
debian.mirror.solnet.ch                                                       /debian/                                 all
ftp.solnet.ch                          /mirror/Debian/debian/                                                          all
debian.ethz.ch                         /debian/                               /debian/                                 all

CL Chile
--------
ftp.cl.debian.org                      /debian/                               /debian/                                 all
debian.experimentos.cl                 /Debian/debian/                        /debian/                                 i386 sparc
debian.ubiobio.cl                                                             /debian/                                 hppa hurd-i386 i386 powerpc sparc

CN China
--------
ftp.linuxforum.net                     /debian/                               /ftp/debian/                             i386
mirrors.geekbone.org                   /debian/                               /debian/                                 all
debian.cn99.com                        /debian/                               /debian/                                 i386

CO Colombia
-----------
fatboy.umng.edu.co                                                            /debian/                                 alpha i386 ia64 powerpc sparc

CR Costa Rica
-------------
debian.efis.ucr.ac.cr                  /debian/                               /debian/                                 all

CZ Czech Republic
-----------------
ftp.cz.debian.org                      /debian/                               /debian/                                 all
debian.sh.cvut.cz                      /debian/                               /debian/                                 all
ftp.zcu.cz                             /pub/linux/debian/                     /ftp/pub/linux/debian/                   i386

DE Germany
----------
ftp.de.debian.org                      /debian/                               /debian/                                 all
ftp2.de.debian.org                     /debian/                               /debian/                                 all
ftp.tu-clausthal.de                    /pub/linux/debian/                     /pub/linux/debian/                       alpha arm i386 ia64 m68k mips mipsel powerpc sparc
debian.uni-essen.de                    /debian/                               /debian/                                 i386
ftp.freenet.de                         /pub/ftp.debian.org/debian/            /debian/                                 all
ftp.uni-erlangen.de                    /pub/Linux/debian/                     /pub/Linux/debian/                       all
sunsite.informatik.rwth-aachen.de      /pub/Linux/debian/                     /ftp/pub/Linux/debian/                   alpha i386 sparc
ftp-stud.fht-esslingen.de              /debian/                               /debian/                                 all
ftp.stw-bonn.de                        /pub/mirror/debian/                                                             all
ftp.fu-berlin.de                       /pub/unix/linux/mirrors/debian/                                                 all
debian.tu-bs.de                        /debian/                               /debian/                                 all
ftp.uni-koeln.de                       /debian/                               /debian/                                 alpha i386 powerpc sparc
debian.pffa.de                         /pub/mirrors/debian/                   /mirrors/debian/                         i386
ftp.mpi-sb.mpg.de                      /pub/linux/distributions/debian/debian/                                         all
ftp.leo.org                            /debian/                               /debian/                                 all
ftp.tiscali.de                         /pub/debian/debian/                    /pub/debian/debian/                      all
debian.serveftp.net                    /debian/                               /debian/                                 i386
ftp.tu-chemnitz.de                     /pub/linux/debian/debian/                                                       all
ftp.uni-stuttgart.de                   /debian/                               /debian/                                 all
ftp.uni-kl.de                          /pub/linux/debian/                     /pub/linux/debian/                       i386
mirrors.sec.informatik.tu-darmstadt.de                                        /debian/debian/                          all
ftp.uni-bayreuth.de                    /pub/linux/Debian/debian/              /linux/Debian/debian/                    alpha i386
ftp.informatik.hu-berlin.de            /pub/Mirrors/ftp.de.debian.org/debian/                                          all
ftp.gwdg.de                            /pub/linux/debian/debian/              /pub/linux/debian/debian/                all

DK Denmark
----------
ftp.dk.debian.org                      /debian/                               /debian/                                 all
mirrors.sunsite.dk                     /mirrors/debian/                       /debian/                                 hppa i386 sparc
ftp.dkuug.dk                           /pub/debian/                                                                    all
mirror.here.dk                                                                /debian/                                 i386 powerpc
debian.uni-c.dk                                                               /debian/                                 all

EE Estonia
----------
ftp.ee.debian.org                      /debian/                               /debian/                                 all

ES Spain
--------
ftp.es.debian.org                      /debian/                               /debian/                                 all
toxo.com.uvigo.es                      /debian/                               /debian/                                 all
ftp.gul.uc3m.es                        /debian/                               /debian/                                 all
ftp.gui.uva.es                         /debian/                                                                        all
ftp.rediris.es                         /debian/                               /debian/                                 all
obelix.umh.es                          /pub/debian/debian/                    /pub/debian/debian/                      i386
jane.uab.es                            /debian/                               /debian/                                 i386
ftp.caliu.info                         /debian/                               /debian/                                 i386 ia64 m68k mips mipsel powerpc sparc
ftp.cica.es                            /debian/                                                                        all
ftp2.caliu.info                        /debian-amd64/debian/                                                           all

FI Finland
----------
ftp.fi.debian.org                      /debian/                               /debian/                                 all
ftp.funet.fi                           /pub/linux/mirrors/debian/             /pub/linux/mirrors/debian/               all
ftp.jyu.fi                             /debian/                               /debian/                                 alpha arm hppa i386 ia64 m68k mips mipsel powerpc s390 sparc

FR France
---------
ftp.fr.debian.org                      /debian/                               /debian/                                 all
ftp2.fr.debian.org                     /debian/                               /debian/                                 all
ftp.iut-bm.univ-fcomte.fr              /debian/                               /debian/                                 all
ftp.eudil.fr                           /debian/                               /debian/                                 all
ftp.proxad.net                         /mirrors/ftp.debian.org/                                                        all
ftp.minet.net                          /debian/                                                                        all
ftp.info.iut-tlse3.fr                  /debian/                               /debian/                                 i386
ftp.lip6.fr                            /pub/linux/distributions/debian/                                                all
debian.ens-cachan.fr                   /debian/                               /ftp/debian/                             i386 sparc
ftp.u-picardie.fr                      /mirror/debian/                        /mirror/debian/                          alpha i386
debian.mirrors.easynet.fr              /debian/                               /                                        alpha i386 powerpc
ftp.u-strasbg.fr                       /debian/                               /debian/                                 all
ftp.ipv6.opentransit.net               /debian/                               /debian/                                 all
debian.lami.univ-evry.fr               /debian/                               /debian/                                 i386 sparc
mirror.cict.fr                         /debian/                               /debian/                                 all
mir1.ovh.net                           /debian/                                                                        all
mir2.ovh.net                                                                  /debian/                                 all
ftp.nerim.net                          /debian/                                                                        i386
ftp.crihan.fr                          /debian/                               /debian/                                 all
ftp.tuxfamily.org                      /debian/                                                                        i386
debian.mines.inpl-nancy.fr             /debian/                               /debian/                                 all
ftp.debian.ikoula.com                  /debian/                                                                        all
ftp.vthd-net.com                       /debian/                               /debian/                                 all

GB Great Britain
----------------
ftp.uk.debian.org                      /debian/                               /debian/                                 all
debian.hands.com                       /debian/                               /debian/                                 all
ftp.demon.co.uk                        /pub/mirrors/linux/debian/                                                      all
ftp.mcc.ac.uk                          /pub/linux/distributions/Debian/                                                all
www.mirror.ac.uk                       /sites/ftp.debian.org/debian/          /sites/ftp.debian.org/debian/            all
ftp.ticklers.org                       /debian/                               /debian/                                 all
ftp.linux.co.uk                        /pub/debian/                                                                    i386
debian.blueyonder.co.uk                /pub/debian/                           /                                        all
mirror.positive-internet.com           /debian/                               /debian/                                 i386
the.earth.li                           /debian/                               /debian/                                 i386
mirror.ox.ac.uk                        /debian/                               /debian/                                 all

GR Greece
---------
debian.otenet.gr                       /pub/linux/debian/                     /debian/                                 all
ftp.ntua.gr                            /pub/linux/debian/                     /pub/linux/debian/                       i386 sparc
ftp.duth.gr                            /debian/                               /debian/                                 all
ftp.softnet.tuc.gr                     /pub/linux/debian/                     /ftp/linux/debian/                       all
debian.spark.net.gr                                                           /debian/                                 i386
debian.internet.gr                     /debian/                               /debian/                                 i386 ia64 s390 sparc

HK Hong Kong
------------
ftp.hk.debian.org                      /debian/                               /debian/                                 all
sunsite.ust.hk                         /pub/debian/                                                                    all
www.zentek-international.com                                                  /mirrors/debian/debian/                  all

HR Croatia
----------
ftp.hr.debian.org                      /debian/                               /debian/                                 all
ftp.irb.hr                             /debian/                               /debian/                                 all
ftp.carnet.hr                          /pub/debian/                           /pub/debian/                             all
debian.iskon.hr                        /debian/                               /debian/                                 all

HU Hungary
----------
ftp.hu.debian.org                      /debian/                               /debian/                                 all
ftp.index.hu                           /debian/                                                                        i386
ftp.kfki.hu                            /pub/debian/                           /debian/                                 all
debian.inf.elte.hu                     /debian/                               /debian/                                 all
ftp.externet.hu                        /debian/                               /debian/                                 i386
ftp.bme.hu                             /OS/Linux/dist/debian/                 /OS/Linux/dist/debian/                   i386 ia64

ID Indonesia
------------
kebo.vlsm.org                          /debian/                               /debian/                                 all
debian.3wsi.net                                                               /debian/                                 i386
debian.indika.net.id                                                          /debian/                                 all
mirrors.chipset.or.id                  /debian/                               /debian/                                 i386
komo.vlsm.org                          /debian/                               /debian/                                 all

IE Ireland
----------
ftp.ie.debian.org                      /debian/                               /debian/                                 all
ftp.esat.net                           /pub/linux/debian/                     /pub/linux/debian/                       all

IL Israel
---------
mirror.hamakor.org.il                                                         /pub/mirrors/debian/                     alpha i386 powerpc sparc

IN India
--------
ftp.iitm.ac.in                         /debian/                               /debian/                                 i386

IS Iceland
----------
ftp.is.debian.org                      /debian/                               /debian/                                 all

IT Italy
--------
ftp.it.debian.org                      /debian/                               /debian/                                 all
ftp2.it.debian.org                     /debian/                               /debian/                                 all
ftp.bononia.it                         /debian/                               /debian/                                 all
ftp.students.cs.unibo.it               /debian/                               /debian/                                 all
freedom.dicea.unifi.it                 /ftp/pub/linux/debian/                 /ftp/pub/linux/debian/                   i386
ftp.edisontel.com                      /pub/Debian_Mirror/                                                             all
debian.nettuno.it                      /debian/                               /debian/                                 i386
ftp.unina.it                           /pub/linux/distributions/debian/debian//pub/linux/distributions/debian/debian/  all
softcity.libero.it                     /debian/                               /debian/                                 all
mi.mirror.garr.it                      /mirrors/debian/                       /mirrors/debian/                         alpha arm hppa i386 ia64 sparc
debian.fastweb.it                      /debian/                               /debian/                                 all

JP Japan
--------
ftp2.jp.debian.org                     /debian/                               /debian/                                 all
ftp.jp.debian.org                      /debian/                               /debian/                                 all
ring.asahi-net.or.jp                   /pub/linux/debian/debian/              /archives/linux/debian/debian/           all
ftp.dti.ad.jp                          /pub/Linux/debian/                                                              all
dennou-t.ms.u-tokyo.ac.jp              /library/Linux/debian/                 /library/Linux/debian/                   all
dennou-k.gaia.h.kyoto-u.ac.jp          /library/Linux/debian/                 /library/Linux/debian/                   all
dennou-q.geo.kyushu-u.ac.jp            /library/Linux/debian/                 /library/Linux/debian/                   all
ftp.yz.yamagata-u.ac.jp                /debian/                               /debian/                                 all
sb.itc.u-tokyo.ac.jp                   /DEBIAN/debian/                                                                 all
ftp.riken.go.jp                        /pub/Linux/debian/debian/              /pub/Linux/debian/debian/                all
debian.shimpinomori.net                                                       /debian/                                 i386
ring.hosei.ac.jp                       /pub/linux/debian/debian/              /archives/linux/debian/debian/           all
www.ring.gr.jp                         /pub/linux/debian/debian/              /archives/linux/debian/debian/           all

KR Korea
--------
ftp.kr.debian.org                      /debian/                               /debian/                                 all
ftp.kornet.net                         /pub/Linux/debian/                     /pub/Linux/debian/                       all
ftp.nuri.net                           /pub/debian/                                                                    all
linux.sarang.net                       /mirror/os/linux/distribution/debian/  /ftp/mirror/os/linux/distribution/debian/i386
ftp.xgate.co.kr                        /pub/mirror/debian/                    /debian/                                 all
mirror.devolus.org                     /debian/                               /debian/                                 all
ftp.kreonet.re.kr                      /pub/Linux/debian/                     /pub/Linux/debian/                       all
ftp.uos.ac.kr                          /mirror/Linux/debian/debian/                                                    all
ftp.sayclub.com                        /pub/debian/                           /pub/debian/                             all

LT Lithuania
------------
ameba.sc-uni.ktu.lt                    /debian/                               /debian/                                 i386
debian.balt.net                        /debian/                               /debian/                                 alpha arm i386 powerpc sparc
debian.vinita.lt                       /debian/                               /debian/                                 i386

LU Luxembourg
-------------
ftp.europeonline.net                   /debian/                                                                        all
debian.luxadmin.org                                                           /debian/                                 i386

LV Latvia
---------
ftp.latnet.lv                          /linux/debian/                         /linux/debian/                           i386 sparc
koyanet.lv                             /debian/                               /ftp/debian/                             i386

MA Morocco
----------
casa.callbright.com                                                           /debian/                                 i386

MX Mexico
---------
nisamox.fciencias.unam.mx              /debian/                               /debian/                                 all

NI Nicaragua
------------
debian.uni.edu.ni                                                             /debian/                                 all

NL Netherlands
--------------
ftp.nl.debian.org                      /debian/                               /debian/                                 all
ftp.nluug.nl                           /pub/os/Linux/distr/debian/                                                     all
ftp.eu.uu.net                          /debian/                               /debian/                                 all
ftp.surfnet.nl                         /pub/os/Linux/distr/debian/            /os/Linux/distr/debian/                  all
download.xs4all.nl                     /pub/mirror/debian/                                                             all
ftp.debian.nl                          /debian/                               /debian/                                 all
ftp.tiscali.nl                         /pub/mirrors/debian/                   /debian/                                 all
debian.essentkabel.com                 /debian/                               /debian/                                 all

NO Norway
---------
ftp.no.debian.org                      /debian/                               /debian/                                 all
debian.marked.no                       /debian/                                                                        all

NZ New Zealand
--------------
ftp.nz.debian.org                      /debian/                               /debian/                                 all
debian.ihug.co.nz                      /debian/                                                                        i386

PL Poland
---------
ftp.pl.debian.org                      /debian/                               /debian/                                 all
ftp.icm.edu.pl                         /pub/Linux/debian/                     /pub/Linux/debian/                       i386
mirror.ipartners.pl                    /pub/debian/                                                                    all
ftp.ps.pl                              /pub/Linux/debian/                     /pub/Linux/debian/                       i386
ftp.man.szczecin.pl                    /pub/Linux/debian/                                                              all

PT Portugal
-----------
ftp.uevora.pt                          /debian/                               /debian/                                 i386
ftp.eq.uc.pt                           /pub/software/Linux/debian/            /software/Linux/debian/                  all
debian.ua.pt                           /debian/                               /debian/                                 all
ftp.cprm.net                           /debian/                               /debian/                                 i386
ftp.linux.pt                           /pub/mirrors/debian/                   /pub/mirrors/debian/                     i386 sparc

RO Romania
----------
ftp.ro.debian.org                      /debian/                               /debian/                                 all
debian.ambra.ro                        /debian/                               /debian/                                 i386
ftp.lug.ro                             /debian/                               /debian/                                 all

RU Russia
---------
ftp.ru.debian.org                      /debian/                               /debian/                                 all
debian.nsu.ru                          /debian/                               /debian/                                 i386
debian.psu.ru                          /debian/                               /debian/                                 hurd-i386 i386
debian.udsu.ru                         /debian/                               /debian/                                 i386
ftp.psn.ru                             /debian/                               /debian/                                 alpha hurd-i386 i386
ftp.corbina.ru                         /pub/Linux/debian/                                                              i386 ia64
debian.samara.ru                                                              /debian/                                 i386

SE Sweden
---------
ftp.se.debian.org                      /debian/                               /debian/                                 all
ftp.sunet.se                           /pub/os/Linux/distributions/debian/    /pub/os/Linux/distributions/debian/      all
ftp.du.se                              /debian/                               /debian/                                 all
kalle.csb.ki.se                        /pub/linux/debian/                     /pub/linux/debian/                       all
mirror.pudas.net                       /debian/                               /debian/                                 all
ftp.port80.se                          /debian/                               /debian/                                 all
ftp.ds.hj.se                           /pub/Linux/distributions/debian/       /pub/Linux/distributions/debian/         i386 powerpc sparc

SG Singapore
------------
ftp.lugs.org.sg                        /debian/                               /debian/                                 i386
mirror.averse.net                      /debian/                               /debian/                                 i386
mirror.nus.edu.sg                      /pub/Debian/                                                                    all
debian.wow-vision.com.sg               /debian/                               /debian/                                 i386

SI Slovenia
-----------
ftp.si.debian.org                      /debian/                               /debian/                                 alpha i386 ia64 m68k powerpc sparc
ftp.arnes.si                           /packages/debian/                                                               all

SK Slovakia
-----------
ftp.sk.debian.org                      /debian/                               /debian/                                 all

TH Thailand
-----------
ftp.nectec.or.th                       /pub/linux-distributions/Debian/                                                all
www.buraphalinux.org                   /pub/debian/                                                                    all
ftp.coe.psu.ac.th                      /debian/                               /debian/                                 all

TR Turkey
---------
ftp.tr.debian.org                      /debian/                               /debian/                                 all
ftp.linux.org.tr                       /pub/mirrors/debian/                                                            all

TW Taiwan
---------
ftp.tku.edu.tw                         /OS/Linux/distributions/debian/        /OS/Linux/distributions/debian/          all
linux.csie.nctu.edu.tw                 /debian/                                                                        all
debian.csie.ntu.edu.tw                 /pub/debian/                           /debian/                                 i386
debian.linux.org.tw                    /debian/                               /debian/                                 all
linux.cdpa.nsysu.edu.tw                /debian/                               /debian/                                 all
opensource.nchc.org.tw                 /debian/                               /debian/                                 all
ftp.isu.edu.tw                         /pub/Linux/Debian/debian/              /pub/Linux/Debian/debian/                all
debian.nctu.edu.tw                     /debian/                               /debian/                                 all
debian.im.nuk.edu.tw                   /pub/debian/debian/                    /pub/debian/debian/                      all

UA Ukraine
----------
debian.osdn.org.ua                     /pub/Debian/debian/                    /debian/                                 i386
debian.org.ua                          /debian/                               /debian/                                 i386
ftp.3logic.net                         /debian/                                                                        i386

US United States
----------------
ftp.us.debian.org                      /debian/                               /debian/                                 all
ftp.debian.org                         /debian/                               /debian/                                 all
debian.crosslink.net                   /debian/                               /debian/                                 all
ftp-linux.cc.gatech.edu                /debian/                                                                        all
ftp.egr.msu.edu                        /debian/                               /debian/                                 all
distro.ibiblio.org                     /pub/Linux/distributions/debian/       /pub/Linux/distributions/debian/         i386 powerpc sparc
ftp-mirror.internap.com                /pub/debian/                           /pub/debian/                             all
ftp.cerias.purdue.edu                  /pub/os/debian/                        /pub/os/debian/                          all
ftp.cs.unm.edu                         /mirrors/debian/                                                                all
mirror.cs.wisc.edu                     /pub/mirrors/linux/debian/             /pub/mirrors/linux/debian/               i386
ftp.uwsg.indiana.edu                   /linux/debian/                                                                  all
debian.tod.net                         /debian/                               /debian/                                 all
natasha.stmarytx.edu                                                          /debian/                                 all
ftp.ndlug.nd.edu                       /debian/                               /mirrors/debian/                         all
debian.uchicago.edu                    /debian/                               /debian/                                 all
carroll.aset.psu.edu                   /pub/linux/distributions/debian/       /pub/linux/distributions/debian/         all
debian.fifi.org                        /pub/debian/                           /debian/                                 alpha i386 ia64 sparc
gladiator.real-time.com                /linux/debian/                                                                  i386
mirrors.kernel.org                     /debian/                               /debian/                                 all
mirrors.xmission.com                   /debian/                               /debian/                                 all
mirrors.rcn.net                        /debian/                               /debian/                                 i386
ftp.keystealth.org                     /debian/                               /debian/                                 alpha i386 ia64 mips mipsel sparc
ftp.stealth.net                        /pub/mirrors/ftp.debian.org/                                                    all
ftp.lug.udel.edu                       /debian/                               /debian/                                 all
debian.lcs.mit.edu                     /debian/                               /debian/                                 all
ftp.rutgers.edu                        /pub/debian/                           /pub/debian/                             all
debian.rutgers.edu                     /pub/                                  /                                        all
archive.progeny.com                    /debian/                               /debian/                                 all
mirror.csit.fsu.edu                    /debian/                               /debian/                                 all
linux.csua.berkeley.edu                /debian/                               /debian/                                 all
ftp.silug.org                          /pub/debian/                           /pub/debian/                             all
debian.secsup.org                      /pub/linux/debian/                     /                                        all
debian.teleglobe.net                   /debian/                               /                                        all
techweb.rfa.org                        /debian/                               /debian/                                 all
debian.oregonstate.edu                 /debian/                               /debian/                                 all
lyre.mit.edu                                                                  /debian/                                 all
wuarchive.wustl.edu                    /mirrors/debian/                       /mirrors/debian/                         i386 ia64
mirror.mcs.anl.gov                     /pub/debian/                           /debian/                                 all
debian.2z.net                                                                 /debian/                                 i386
sluglug.ucsc.edu                       /debian/                               /debian/                                 all
ftp.cs.stevens-tech.edu                /pub/Linux/distributions/debian/                                                all
slugsite.louisville.edu                /debian/                               /debian/                                 all
cudlug.cudenver.edu                    /debian/                               /debian/                                 i386 ia64 powerpc
mirrors.geeks.org                      /debian/                                                                        i386 ia64 powerpc sparc
mirrors.engr.arizona.edu                                                      /debian/                                 i386
mirrors.terrabox.com                   /debian/                               /debian/                                 all
debian.midco.net                       /debian/                               /debian/                                 all
mirrors.usc.edu                        /pub/linux/distributions/debian/       /pub/linux/distributions/debian/         all
debian.mirrors.pair.com                /                                      /                                        all
lug.mtu.edu                            /debian/                               /debian/                                 alpha hppa i386 mips mipsel powerpc sparc
ftp.tomstroubleshooting.com            /debian/                                                                        i386
debian.mirrors.tds.net                 /debian/                               /debian/                                 all

ZA South Africa
---------------
ftp.is.co.za                           /debian/                               /debian/                                 i386
ftp.linux.co.za                        /pub/distributions/debian/                                                      all
ftp.sun.ac.za                          /debian/                               /ftp/debian/                             i386 sparc

-------------------------------------------------------------------------------
Last modified: Tue May 24 18:52:12 2005             Number of sites listed: 372
