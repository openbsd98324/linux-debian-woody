
		    The Debian Installation System
                    ==============================

                             Orientation

  This README is to acquaint you with the contents of the Debian
  installation system, and where to go for more information.

  If you wish to return to this directory you should go to

    <debian>/dists/woody/main/disks-i386/current/

  where the word `<debian>' may indicate a Debian web mirror, an FTP
  area, or official Debian CD-ROM.  All the files required for
  installation can be found beneath this directory.

  Complete documentation for the Debian installation system is located
  within the `doc/' subdirectory.  Documentation is available in
  several formats and languages.  You may also wish to consult
  <URL:http://www.debian.org/releases/woody/> for errata, security
  alerts, and other updated information.

  For the impatient, quick install instructions are provided here.
  However, the reader is *strongly* encouraged to read the full
  documentation found in the `doc/' subdirectory, especially in the
  event of difficulties.



                   Quick Installation Instructions
                   ===============================

  Even though the Debian Installation System is also called the
  `boot-floppies', this name is something of an historical artifact.
  It is possible, even desirable, to install Debian without the use of
  floppies at all.  For instance, you may be able to install Debian
  from an official Debian bootable CD-ROM, from the network, or from
  another operating system.


  CD-ROM Install
  -------------

  The recommended installation method is the official Debian CD-ROM.
  These may be procured anywhere fine GNU/Linux distributions are
  sold, or online <URL:http://www.debian.org/distrib/vendors>.

  Those who have a CD burner and an adventurous disposition may try
  creating their own CD-ROM image <URL:http://cdimage.debian.org/>.

  Installation from CD-ROM is usually very easy and requires you to
  type only a single command after booting and CD insertion. If your
  machine has SRM (i.e. no menues system at the beginning) simply
  type `boot xxx -fl 0` where xxx is your CD-ROM drive in SRM
  notation. Run `show dev` to find out the name in your machine. For
  a complete description check out the installation manual.

  If your machine has ARC or AlphaBIOS, see below in the MILO section
  for details.


  Installation from Floppies
  --------------------------

  If you either do not have a CD-ROM or you have a computer that
  cannot boot from a CD-ROM then you will have to use the floppy
  images located beneath this directory.  Floppy images are named
  `*.bin'.  You will generally need the `rescue.bin' and `root.bin'
  images, and often the `driver-*.bin' images as well.

  Note that there are various sets of these rescue, root, and driver
  images, depending on which architecture you are on.  You will have
  to choose a set with which to install.  See below for a detailed
  description of the various subdirectories which contain these floppy
  images.

  The `driver-*.bin' files contain extra hardware drivers not
  contained in the kernel in the `rescue.bin' image.  It is not always
  required to create floppies from which to load these drivers, e.g.,
  in cases where the installation is able to get the drivers from
  non-floppy media.  It's basically a question of how much hardware
  support the kernel itself contains.

  If you do not create the `driver-*.bin' floppies, later during the
  installation you will need a copy `rescue.bin' and `drivers.tgz' to
  be available from non-floppy media.  You will be prompted for the
  operating system and modules, and you cannot tell the system to use
  floppies, obviously, since you did not create the driver floppies.
  Direct the installer to get this data off the CD-ROM, local hard
  disk, or wherever the installation files were downloaded.

  You *cannot* create floppies from *.bin files by just copying the
  *.bin files over.  You need to do a low-level sector copy of the
  data onto the floppy.  The method you must use to create floppies
  from the *.bin files varies based on what operating system you have
  access to.  Descriptions below cover creating floppies from *.bin
  files in Unix and from DOS.

  It is recommended that you always use fresh new floppies, because
  used ones can contain errors and cause failures in booting.

  - Creating Floppies from Floppy Images in Unix

    Use the GNU `dd' command to create a floppy disk from a .bin file:

        dd of=/dev/fd0 if=<file> bs=1024

    Your floppy device may be something other than `/dev/fd0'.

    If you are naturally suspicious, you can make sure the image was
    successfully written:

        cmp /dev/fd0 <file>

  - Creating Floppies from Floppy Images in DOS

    Change directory (`cd') to the directory containing the *.bin
    files that you want.  Use the DOS utility `rawrite2.exe' in the
    `dosutils' subdirectory to create the image, for instance:

		..\dosutils\rawrite2 -f rescue.bin -d a

     where `a' refers to the first floppy drive on your computer, and
     `rescue.bin' represents the *.bin file from which you want to 
     create a floppy.


  Installing From Within DOS
  --------------------------

  If you do not have an official CD-ROM, and are running DOS or
  Windows, there is an alternative to creating floppies.  You may run
  the DOS batch file `install.bat' from one of the top-level
  subdirectories.

  See below for detailed description of the various subdirectories
  which contain 'install.bat'.






                   Layout Of The Installation Files
                   ================================

  The general organization of files in this directory is described
  below.  If you are copying a subset of these files to local disk or
  what have you, you should retain the internal directory structure,
  since the installation system will be looking for files in these
  locations.

  doc/

      The Debian GNU/Linux Installation Manual, the Beginner's Guide for
      `dselect', and the Release Notes may be found here in several
      computer readable and printable formats.  Please, Read The Fine
      Manual (RTFM) before you begin!

  <flavor>/

      There are several `flavors' of installation disk available.  In
      some cases the images contain a Linux kernel compiled with
      certain options that make it work better on some hardware.  See
      below for information about why you might need to use a flavor.

      Also in this directory are files for a particular flavor which
      are not disk images, but may be helpful for network
      installations or installations from another operating system.

      During a network, NFS, or CD-ROM install, the install software
      knows how to find these files, once you have indicated the
      <debian> directory.  If you plan to copy these files to a spot
      on your hard drive in anticipation of using the installer's
      "from a mounted partition" option, you do not need to duplicate
      the directory structure of the <debian> archive, but you do need
      to make sure you get a matched set of images, all of the same
      <flavor>, or things probably won't work correctly.

      The flavors available for this architecture are `compact', 'idepci',
      and `ide'.

      compact .... A Linux kernel with some non-critical device
                   drivers removed, and a few of the more common PCI
                   device drivers compiled into the kernel itself.
                   See images-1.44/compact/README.txt

      idepci ....  Similar to compact, but even more PCI device drivers
                   are compiled into the kernel, and SCSI is removed.
                   See images-1.44/idepci/README.txt

      ide    ..... Specialized kernel for those who require the UDMA66
                   IDE patch.  This may be needed if you have a
                   Promise Ultra66 IDE controller, among others.
                   See images-1.44/ide/README.txt

  images-<size>/<flavor>/

      Disk images of size <size>.  Choose the size that will fit on the
      media from which you intend to bootstrap the installation, and
      follow the instructions below under "Writing Image Files to
      Floppies".


  ** Specific Files of Interest

  Using the descriptions above, you need to select the directory
  containing the set of files which is appropriate to the installation
  you are doing.  You will need all of the following `.bin' images,
  unless marked otherwise.

  .../rescue.bin

      Rescue disk image, containing the kernel and a boot loader.

  .../root.bin

      Root disk image, containing the root file system.  Not required
      unless are you are installing from floppies.

  .../driver-#.bin

      Device driver disk images, containing kernel modules you can
      load for hardware for which there is not a driver built into the
      kernel.  For instance, you can use this to install a driver for
      your network adapter; once you have installed that driver, you
      can install the rest of the system over the network.  Other
      modules include PPP, parallel support, etc.  Not required unless
      are you are installing from floppies.

  .../drivers.tgz

      A compressed tar archive containing the same modules as the
      above disk images.  These are used when installation kernel and
      drivers from local disk or CD rather than from floppies.  Use
      the file from the appropriate subdirectory based on the
      flavor you are using, if any.


  .../install.bat

      DOS batch script for booting into the installation system from
      DOS.  A different version of this batch file is available for
      each flavor.

  .../linux.bin

      A Linux kernel image, used by the batch script above.

  md5sum.txt

      A file containing MD5 sums for installation files.  This can be
      used to verify that downloaded files have not been corrupted.














