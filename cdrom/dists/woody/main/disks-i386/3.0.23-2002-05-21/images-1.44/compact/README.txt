The compact flavor contains a thinner, trimmer, stripped down kernel
for the boot floppies.  It is geared towards modern PCs.

It does not include support for math emulation, sound, joysticks,
video4linux, and other drivers not essential for installation.  In
addition, this kernel lacks support for the microchannel bus, APM,
ARCnet, and other devices which may be supported by the vanilla
kernel.

It includes support for PCI SCSI and IDE controllers, some PCI RAID
controllers (including DAC960 and SMART2) and some network controllers.

If you do not have SCSI, you might want to try the 'idepci' flavor.

In terms of installation, this set of floppies should work just as well
as the regular ones. However, once your system is installed, you may
wish to install the full kernel image package to get access to other
available kernel modules.

See the file 'kernel-config' for the complete list of devices included
in this flavor.

See the top-level README.txt file for a description of what the files
in this directory are for.  To get a complete description of what
files are required for various installation methods, see the
Installation Manual.
