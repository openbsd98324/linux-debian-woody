Vi povas meti iun ajn densigitan linuksan kernon sur �i tiu disketo,
kaj �i devus starti. Por fari tion:
    Agordu la kernon kun la sekvaj kapabloj enligitaj: initrd,
      ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Konstrui vian kernon per "make bzImage".
    Kopiu �in al "linux" sur la startdisketo.
    �angu la dosierujon al tiu de la startdisketo kaj ruligu ./rdev.sh
      por agordi la kernon.
    La�bezone redaktu syslinux.cfg por aldoni argumentojn al la linio
      "DEFAULT", a� aldonu linion "APPEND" kun argumentoj por aldoni al
      iu komando tajpita de la uzanto krom la implicita elekto.

Dokumentoj por legi:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Fontprogramo:
    La programetoj, kiuj kreis �i tiun disketon kaj la aliajn
    startdisketojn de Debian estas instalitaj en
    /usr/src/boot-floppies de la pako boot-floppies.

- Bruce Perens, 1996-03-12 [tradukis EGE, 1999-12-01]
