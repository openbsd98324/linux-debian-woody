
JO� POSEBNIH PARAMETARA DIZANJA

Na boot: promptu mo�ete koristiti slijede�e parametre dizanja.
Ako koristite heksadecimalne brojeve, ispred njih stavite "0x" (npr. 0x300).

�titi podrucja I/O portova   reserve=iobaza,opseg[,...]
Adaptec 151x, 152x           aha152x=iobaza[,irq[,scsi-id[,ponovnospajanje]]]
Adaptec 1542                 aha1542=iobaza[,busda,busne[,dmabrzina]]
Adaptec 274x, 284x           aic7xxx=bez_reseta (uklju�eno ako nije nula)
BusLogic SCSI Hosts          buslogic=iobaza
Future Domain TMC-8xx/950    tmc8xx=mem_baza,irq
Pro Audio Spectrum           pas16=iobaza,irq
Seagate ST-0x                st0x=mem_baza,irq
Trantor T128                 t128=mem_baza,irq
ATAPI/IDE CD-ROM             hd{a,b,c,d}=cdrom   (obi�no nepotrebno)

Moduli:
Driveri za Ethernet ure�aje i vlastita CD-ROM su�elja se u�itavaju kao
moduli, pa za njih ne mo�ete navesti parametre na boot: promptu.




