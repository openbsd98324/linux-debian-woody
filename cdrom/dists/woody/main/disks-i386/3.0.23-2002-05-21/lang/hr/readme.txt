Na ovu disketu mo�ete smjestiti bilo kakav sa�eti Linux kernel i trebao bi
se di�i. Za to je potrebno:
    Konfigurirajte kernel sa ugra�enim slijede�im mogu�nostima: initrd,
	ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Izgradite svoj kernel pomo�u "make bzImage".
    Kopirajte ga u "linux" na boot disketi.
    Promijenite direktorij na boot disketu i pokrenite ./rdev.sh kako bi
    se kernel konfigurirao.
    Mo�ete i editirati syslinux.cfg te dodati argumente redu DEFAULT,
	ili dodati red APPEND s argumentima koji �e biti dodani svakoj
	naredbi koju korisnik upi�e, zajedno s predodre�enima.

Dokumentacija za �itanje:
    /usr/lib/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Izvorni kod:
    Skripte koje stvaraju ovu disketu i ostale diskete za dizanje Debiana
    paket boot-floppies instalira u /usr/src/boot-floppies/.

-- Bruce Perens, 12. o�ujka 1996.
