0fINDICE DE AYUDA:07

Pulse las teclas de funci�n <09F107> a <09F1007> para ver informaci�n de ayuda.

0fTECLA  TEMA07

<09F107>   Esta p�gina, el �ndice de ayuda.
<09F207>   Prerequisitos para instalar este sistema.
<09F307>   M�todos de arranque especiales.
<09F407>   Par�metros de arranque, resumen.
<09F507>   Par�metros de arranque para m�quinas especiales.
<09F607>   Par�metros de arranque para ciertos controladores de disco.
<09F707>   Par�metros de arranque para el sistema de instalaci�n.
<09F807>   Como obtener ayuda.
<09F907>   Listas de correo de Debian.
<09F1070>  Copyrights y Garant�as




Pulse <09INTRO07> o introduzca par�metros de arranque y despu�s <09INTRO07>
para arrancar. Pulse de <09F207> a <09F1007> para ver la ayuda r�pida para la
instalaci�n.

