Puede poner cualquier kernel Linux comprimido en este disco, y debe arrancar.
Para hacerlo:
    Configure el kernel con las siguientes caracter�sticas en �l: initrd,
      ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Comp�lelo con "make bzImage".
    C�pielo con el nombre "linux" en el disco de arranque.
    Cambie de directorio al disco de arranque y ejecute ./rdev.sh para
      terminar de configurar el kernel.
    Opcionalmente, edite syslinux.cfg (en el mismo disco de arranque)
      para a�adir argumentos en la linea "DEFAULT", o a�adir una linea
      "APPEND" con argumentos que ser�n a�adidos a cualquier linea de
      �rdenes que introduzca el usuario, as� como a la linea de �rdenes
      predeterminada.

Documentaci�n que debe leer:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

C�digo fuente:
    Los scripts que crean este disco y los dem�s discos de instalaci�n de
    Debian se instalan en /usr/src/boot-floppies/ con el paquete
    boot-floppies.

- Bruce Perens, 12-March-1996. Enrique Zanardi, 20-Agosto-1998
