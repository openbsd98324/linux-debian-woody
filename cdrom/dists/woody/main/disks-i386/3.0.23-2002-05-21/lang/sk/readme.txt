Sem, na disk, m��ete umiestni� hocak� komprimovan� jadro Linuxu, a malo by �s�.
N�vod:
    Nakonfigurujte jadro, aby obsahovalo zlinkovan� nasledovn� �asti: initrd,
	ramdisk, loop, msdos, fat, minix, elf, ext2fs, procfs.
    Vytvorte kernel pomocou "make bzImage".
    Nakop�rujte ho ako "linux" na zav�dzac� disk.
    Zme�te adres�r na zav�dzac� disk a spustite ./rdev.sh pre jeho konfigur�ciu.
    Volite�ne, upravte syslinux.cfg a pridajte parametre do riadku "DEFAULT"
	alebo pridajte riadok "APPEND" s parametrami, ktor� bod� pridan� k
	u��vate�sk�mu pr�kazov�mu riadku tak ako �tandardn�.

Mali by ste si pre��ta� nasledovn� dokument�ciu:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Zdrojov� k�d:
    Skripty, ktor� vytv�raj� tento disk a ostatn� zav�dzacie disky Debianu
    s� in�talovan� v /usr/src/boot-floppies/ po in�tal�cii bal�ku boot-floppies.

- Bruce Perens, 12. marca 1996
