
Z�SKANIE N�POVEDY

Ak nem��ete nain�talova� syst�m, nez�fajte! T�m Debianu je pripraven� v�m
pom�c�! Zvlṻ sa zauj�mame o probl�my s in�tal�ciou, preto�e sa vo
v�eobecnosti nest�vaj� iba _jednej_ osobe. Bu� sme u� po�uli o probl�me
podobnom tomu v�mu a m��me poskytn�� r�chlu opravu alebo sa o �om
radi od v�s dozvieme a naprav�me ho spolu s vami a �a��� u��vate�, ktor�
pr�de s t�m ist�m probl�mom, bude ma� z va�ej sk�senosti ��itok!

Ak m�te pr�stup na WWW, mali by ste sa najprv pozrie� na na�u Web-str�nku
http://www.debian.org/ alebo http://www.debian.cz/


T�m Debianu m��ete kontaktova� cez Internet, pridan�m sa do diskusnej
skupiny debian-users. Na pripojenie sa, po�lite elektronick� po�tov�
spr�vu na adresu debian-user-REQUEST@lists.debian.org s detailn�m popisom
v�ho probl�mu, spolu s textom hociakej syst�movej chyby alebo spr�vy
o ktorej sa domnievate, �e by mohla by� spojen� s va��m probl�mom. Va�u
spr�vu bud� ��ta� stovky u��vate�ov Debianu, a niekto v�m ur�ite pom��e.


Stla�te <ENTER> alebo nap��te parametre a potom <ENTER> pre zavedenie syst�mu.
Stla�te funk�n� kl�vesu <F1> pre index n�povedy.

