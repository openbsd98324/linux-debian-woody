Podeu ficar qualsevol nucli Linux comprimit en aquest disc i hauria
d'arrencar. Per fer-ho:
    Configureu el nucli amb les seg�ents funcionalitas compilades dins:
	initrd, ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Compileu el nucli amb "make bzImage".
    Copieu-ho a "linux" en el disc d'arrencada.
    Canvieu de directori al disquet d'arrencada i executeu ./rdev per
	configurar el nucli.
    Opcionalment, editeu syslinux.cfg per afegir arguments a la l�nia "DEFAULT"
	o afegiu una l�nia "APPEND" amb arguments a afegir a qualsevol ordre
	teclejada pel usuari a mes de les opcions per defecte.

Documentaci� per a llegir:
    /usr/share/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Codi font:
    Els scripts que creen aquest disc i els altres discs d'arrencada de Debian
    s'instal�len en /usr/src/boot-floppies/ amb el paquet boot-floppies.

- Bruce Perens, 12-mar�-1996
