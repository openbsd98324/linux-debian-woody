Voc� pode colocar qualquer kernel compactado do linux neste disco e este dever� inicializar.
Para faz�-lo:
	Configure o kernel com as seguintes op�oes: initrd,
	ramdisk, loop, msdos, fat, minix, elf, ext2fs, procfs.
	Crie seu kernel com o "make bzImage".
	Copie o kernel para o arquivo "linux" no disco de inicializa�ao.
	Mude para o diret�rio do disco de inicializa�ao e rode ./rdev.sh para configurar o kernel.
	Opcionalmente, edite o syslinux.cfg para adicionar argumentos na linha 
	"DEFAULT", ou adicione uma linha  "APPEND" com argumentos a serem 
	adicionados a qualquer linha de comando do usu�rio assim como a default.

Documenta�ao a ser lida:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

C�digo Fonte:
 Os scripts que criam este disco ou outro disco bootstrap da Debian estao 
 instalados no /usr/src/boot-floppies/ pelo pacote boot-floppies.

- Bruce Perens, 12-March-1996
