0fLISTAS DE DISCUSSAO POR E-MAIL DA DEBIAN07

Caso tenha acesso WWW, verifique primeiro a p�gina 0fhttp://www.debian.org.br/07, 
siga at� o link chamado 0fMailing List Subscription07 para ter uma visao
das mais de 80 listas associadas com o projeto da Debian. Um formul�rio de 
inscri�ao esta disponivel na p�gina 0fMailing List Subscription07.

Se conhecer o nome da lista, voc� pode se inscrever manualmente enviando um 
e-mail para 0fdebian-<nomedalista>-REQUEST@lists.debian.org07 contendo 
a palavra 0fsubscribe07. A comunica��o com outros inscritos a lista � 
feita atrav�s do endere�o de e-mail:
0fdebian-<nomedalista>@lists.debian.org07. 
Onde <nomedalista> � substituida pelo nome da lista de e-mail de interesse.

Por exemplo, se desejar relatar um problema ou precisa de ajuda na instala�ao 
dos pacote da Debian, envie a palavra 0fsubscribe07 em uma mensagem para 
0fdebian-user-portuguese-REQUEST@lists.debian.org07; entao envie uma 
explica�ao detalhada do seu problema para:
0fdebian-user-portuguese@lists.debian.org07. 
Sua mensagem ser� lida por centenas de usu�rios da Debian e alguem te ajudara. 

Pressione <09ENTER07> ou digite o m�todo de inicializa�ao, op�oes e <09ENTER07> 
para inicializar.
Pressione a tecla de fun�ao <09F107> para retornar ao indice de ajuda.
