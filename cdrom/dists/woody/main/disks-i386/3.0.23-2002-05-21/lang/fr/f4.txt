0fPARAM�TRES DE D�MARRAGE PARTICULIERS07

Sur certaines machines, vous pourriez avoir � sp�cifier un param�tre � l'invite
0fboot:07 pour pouvoir d�marrer le syst�me. Par exemple, Linux pourrait ne pas
�tre capable d'� autod�tecter � votre mat�riel et vous aurez a sp�cifier
explicitement son emplacement ou son type pour qu'il puisse �tre reconnu. Le
tableau ci-dessous indique si vous avez besoin d'un param�tre. Si c'est le
cas, entrez la 0fm�thode de d�marrage07 suivie par le param�tre du tableau. Par
exemple, si votre lecteur de disquette 3.5" est votre second lecteur et que
votre premier lecteur lit des disquettes 5.25", vous taperiez
� 0flinux root=/dev/fd107 � � l'invite 0fboot:07.  Quelques arguments
requi�rent des param�tres num�riques montr�s en 0bbleu clair07. Si vous
utilisez des nombres hexad�cimaux, vous devez utiliser le pr�fixe 0x
(par ex 0x300).

Pour plus d'informations sur les param�tres � utiliser, tapez :

 <09F507> -- param�tres de d�marrage pour certaines machines particuli�res
 <09F607> -- param�tres de d�marrage pour certains contr�leurs disques
 <09F707> -- param�tres pour la proc�dure d'installation


Appuyez sur <09Entr�e07> ou tapez les param�tres de d�marrage puis <09Entr�e07>.
Appuyez sur la touche de fonction <09F107> pour le sommaire de l'aide.
