Vous pouvez placer n'importe quel noyau Linux comprim� sur ce disque,
il pourra d�marrer. Pour le faire :
    Configurez le noyau avec les fonctionnalit�s suivantes :
      initrd, ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Cr�ez votre noyau avec � make bzImage �.
    Copiez le � la place de � linux � sur la disquette de d�marrage.
    Placez-vous dans le r�pertoire du m�dia de d�marrage et lancer ./rdev.sh
      pour configurer le noyau.
    �ventuellement, modifiez syslinux.cfg pour ajouter des arguments � la
      ligne � DEFAULT �, ou ajoutez une ligne � APPEND � avec les arguments
      � ajouter aussi bien � toute ligne de commande utilisateur qu'� celle
      par d�faut.

Documentation � lire :
    /usr/share/doc/syslinux/syslinux.doc.gz
    � man rdev �
    /usr/src/linux/Documentation/ramdisk.txt

Code source :
    Les scripts qui cr�ent cette disquette et les autres disquettes de
    d�marrage Debian sont install�s dans le r�pertoire /usr/src/boot-floppies/
    par le paquet boot-floppies.

- Bruce Perens, 12 mars 1996
- Traduction fran�aise : Christophe Le Bars, Vincent Renardias, Pierre Machard.
