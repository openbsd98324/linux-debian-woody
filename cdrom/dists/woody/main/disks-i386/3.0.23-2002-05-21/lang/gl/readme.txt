Pode armacenar calquera n�cleo de Linux comprimido neste disco, e deber�a
arrincar. Para facelo:
    Configure o n�cleo coas seguintes caracter�sticas: initrd, ramdisk,
        loop, msdos, fat, elf, ext2fs, procfs.
    Faga o n�cleo con "make bzImage".
    C�pieo a "linux" no disco de inicio.
    Cambie de directorio ao disco de inicio e execute ./rdev.sh para
        configura-lo n�cleo.
    Opcionalmente, edite syslinux.cfg para engadir argumentos � li�a "DEFAULT"
        ou engada unha li�a "APPEND" con argumentos que se han engadir a
        calquera comando tecleado polo usuario, e tam�n �s opci�ns por defecto.

Documentaci�n para ler:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

C�digo fonte:
    Os scripts que crean este disco e os outros discos de inicio de Debian
    inst�lanse en /usr/src/boot-floppies/ gracias ao paquete boot-floppies.

- Bruce Perens, 12-Marzo-1996
- Jacobo Tarr�o, 10-Abril-2001
