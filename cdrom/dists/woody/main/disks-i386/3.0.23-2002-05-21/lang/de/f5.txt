WEITERE SPEZIELLE BOOT-PARAMETER

Sie k�nnen die folgenden Startoptionen an der 'boot:'-Eingabeaufforderung
in Kombination mit der Angabe einer Bootmethode benutzen (siehe F3). Hexa-
dezimalzahlen m�ssen die Zeichen 0x vorangestellt werden.

HARDWARE                                ANZUGEBENDER PARAMETER
Monochromer Monitor                     mono
IBM PS/1 oder ValuePoint (IDE Platte)   hd=<Zylinder>,<K�pfe>,<Sektoren>
IBM ThinkPad                            floppy=thinkpad
IBM Pentium Microchannel                mca-pentium no-hlt
IO-Adressen ausschliessen               reserve=iobase,extent[,...]
Fehlerhafter math. Co-Prozessor         no387
Advanced Power Management einschalten   apm=on

Ethernet und CDROM
Ethernet-Treiber sowie Treiber f�r propriet�re CD-ROM-Interfaces werden als 
Module geladen.  F�r solche Treiber geben Sie die Parameter w�hrend der
Installation und nicht beim Booten an. Lesen Sie auch die Installations
Anleitung f�r n�here Details.

Dr�cken Sie <ENTER> um zu booten. Bei Bedarf geben Sie zuvor Boot-Parameter an.
Mit der Funktionstaste <F1> erhalten Sie den Hilfe-Index.

