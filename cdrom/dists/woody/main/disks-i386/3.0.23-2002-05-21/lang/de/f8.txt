Zus�tzliche Hilfe

Wenn Sie das System nicht installieren k�nnen, geben Sie bitte nicht einfach 
auf. Wir w�rden sehr gerne von Ihren Schwierigkeiten h�ren, weil diese in der
Regel nicht bei einem einzigen System allein auftreten. Eventuell kennen wir 
das Problem bereits und k�nnen Ihnen eine L�sung mitteilen oder wir k�nnen es
mit Ihnen zusammen l�sen, so dass der n�chste Benutzer von unserer Erfahrung 
profitieren kann. 

Probleme k�nnen grunds�tzlich in zwei Kategorien eingeteilt werden: Probleme 
bei der Installation des Basissystems und Probleme bei der Installation von 
Paketen (nach der Installation des Basissystems). 

Von Schwierigkeiten bei der Installation des Basissystems sollten Sie dem 
Debian Boot-Floppy-Team berichten (debian-boot@lists.debian.org). Probleme
bei der Installation von Paketen sollten an die folgende Adresse berichtet
werden: debian-user@lists.debian.org. Ihr Fehlerbericht sollte in jedem Fall
eine ausf�hrliche Beschreibung des Problems mit den entstandenen Fehler-
meldungen enthalten. Er muss in englischer Sprache verfasst werden. Dr�cken
Sie F9, um weitere Hinweise zu den Debian-Mailinglisten zu erhalten.

Dr�cken Sie <ENTER> um zu booten. Bei Bedarf geben Sie zuvor Boot-Parameter an.
Mit der Funktionstaste <F1> erhalten Sie den Hilfe-Index.

