Jeder komprimierte Linux-Kernel kann auf diese Diskette kopiert werden und
sollte dann gestartet werden k�nnen.

Dazu sind die folgenden Schritte durchzuf�hren:
    Konfigurieren Sie den Kernel mit folgenden Treibern: initrd,
	ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Erzeugen Sie den Kernel mit "make bzImage".
    Kopieren Sie den Kernel nach "linux" auf dieser Diskette.
    Wechseln Sie das Verzeichnis auf diese Diskette und geben
	Sie dann ./rdev.sh ein, um den Kernel zu konfigurieren.
    Ggf. editieren Sie syslinux.cfg um Argumente zur Zeile "DEFAULT"
	hinzuzuf�gen oder f�gen Sie eine Zeile "APPEND" mit Argumenten 
	an, die zu den Benutzereingaben hinzugef�gt werden.

Lesenswerte Dokumentation:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Quellcode:
    Die Skripts, die diese und die anderen Debian Boot-Disketten erzeugen, 
    werden vom Paket "boot-floppies" nach /usr/src/boot-floppies/ installiert.

- Bruce Perens, 12/03/1996
