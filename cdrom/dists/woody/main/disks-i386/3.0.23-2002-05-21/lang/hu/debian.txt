
          0f�dv�zl�nk a 0bDebian GNU/Linux 3.0 rendszerben0f!07

Ez a Debian Rescue lemez. Ezt tartsd meg akkor is, ha siker�lt 
feltelep�teni a rendszert, mert ezzel tudod a merevlemezes rendszert
jav�tani, ha ez valaha sz�ks�gess� v�lna (09<F3>07 a r�szletek�rt)

A legt�bb rendszeren egyszer�en 09<ENTER>07-t �thetsz, a telep�t�s elkezd�s�hez.
Ezt �rdemes kipr�b�lnod, miel�tt b�rmi m�sba kezdesz. Ha valamilyen probl�ma
mer�l fel, vagy ha m�r most van k�rd�sed, akkor az 0f<F1>07 billenty�vel 
gyors telep�t�si �tmutat�t kaphatsz.

0fFIGYELMEZTET�S07: Aj�nlott a telep�t�s el�tt a teljes merevlemezed
  lement�se. A telep�t�si proced�ra v�g�rv�nyesen �s 0cvissza�ll�thatatlanul07
  t�r�lhet minden adatot! Ha m�g nem csin�lt�l m�solatot, akkor vedd ki
  a rescue lemezt a meghajt�b�l, �s nyomd meg a <09RESET07> vagy a 
  <09Control-Alt-Del07> gombokat a r�gi rendszeredbe val� visszat�r�shez.

A 0bDebian GNU/Linux07 SEMMIF�LE GARANCI�T NEM V�LLALUNK, a jog keretein 
bel�l. Tov�bbi copyright inform�ci�k�rt, nyomd meg az <09F1007> billenty�t.

Ez a lemez a Linux  verzi�j�t haszn�lja
    (__kernel_image__ csomagb�l)

<09F107> gomb a s�g�hoz, vagy <09ENTER07> a rendszerind�t�shoz!
