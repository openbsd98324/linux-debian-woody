Bu disketin uzerine herhangi bir sikistirilmis linux cekirdegini
yerlestirebilirsiniz, ve bunun calismasi gerekli.
Bunu yapmak icin:
    Cekirdegi su ozellikleri icerecek sekilde ayarlayin: initrd,
	ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
    Cekirdeginizi "make bzImage" ile derleyin.
    Boot disketi uzerine "linux" olarak kopyalayin.    
    Calisma dizinini boot disketine ayarlayin ve cekirdegi ayarlamak icin
        ./rdev.sh komutunu calistirin.
    Opsiyonel olarak, syslinux.cfg dosyasinda "DEFAULT" satirina degerler
    ekleyin veya default uzerine her kullanicinin kullandigi komuta eklenecek
    secenekleri "APPEND" satirina girin.

Okunacak dokumanlar:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/documentation/ramdisk.txt

Kaynak kod:
    Bu disketi ve diger Debian bootstrap disketlerini olusturan scriptler
    /usr/src/boot-floppies/ altina boot-floppies paketi tarafindan
    yerlestirilmistir.

- Bruce Perens, 12-March-1996
