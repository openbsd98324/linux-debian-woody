0fSPOU�T�C� METODY07

Z�chrannou disketu lze pou��t k oprav� existuj�c�ho syst�mu nebo k
nov� instalaci. Parametry 0flinux07, 0framdisk07, a
0ffloppy07 slou�� ke spu�t�n� instala�n�ho syst�mu. Parametrem
0frescue07 p�ipoj�te existuj�c� diskov� odd�l.

Dostupn� spou�t�c� metody:
0flinux07
  Spust� v�choz� instalaci. P�ednastaven� volba.
0framdisk007 (nebo 0framdisk107)
  Spust� instalaci a na�te ramdisk z prvn� (nebo druh�) disketov�
  jednotky
0ffloppy007 (nebo 0ffloppy107)
  Spust� instalaci a p�ipoj� ko�enov� syst�m z prvn� (nebo druh�) disketov�
  jednotky.
  (M�-li syst�m jen jednu disketovou jednotku, nelze ��st z�kladn�
  syst�m z floppy0, je mo�n� ho instalovat z jin�ho m�dia (CD, pevn� disk ...)
0frescue07
  Spust� syst�m a p�ipoj� libovoln� ko�enov� syst�m soubor�, kter�
  se mus� zadat na p��kazov�m ��dku, nap�.: 0frescue root=/dev/hda107.


Syst�m se spust� stiskem <09ENTER07> �i zad�n�m spou�t�c�ch parametr� a <09ENTER07>.
Rejst��k n�pov�dy se zobraz� stiskem kl�vesy <09F107>.


