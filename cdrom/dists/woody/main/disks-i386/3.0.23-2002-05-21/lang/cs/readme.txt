Na disketu lze um�stit libovoln� zkomprimovan� j�dro Linuxu a to potom spustit.
Toho se doc�l� t�mto postupem:
    Zkonfigurujte j�dro s nastaven�mi odkazy na n�sleduj�c�mi prost�edky:
       initrd, ramdisk, loop, msdos, fat, elf, ext2fs a procfs.
    P��kazem "make bzImage" vytvo�te vlastn� j�dro.
    J�dro zkop�rujte do souboru "linux" na spou�t�c� disket�.
    P�epn�te se na spou�t�c� disketu a spu�t�n�m ./rdev.sh j�dro zkonfigurujte.
    Voliteln� upravte syslinux.cfg p�id�n�m argument� na ��dek "DEFAULT" nebo
    p�idejte ��dek "APPEND" s argumenty, kter� se maj� p�ipojit za libovoln�
    u�ivatelem zadan� p��kazov� ��dek i ��dek v�choz�.

Doporu�en� dokumentace:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Zdrojov� k�d:
    Skripty vytv��ej�c� tuto disketu i jin� spou�t�c� diskety syst�mu Debian
    jsou nainstalov�ny do /usr/src/boot-floppies/ bal��kem boot-floppies.

- Bruce Perens, 12. b�ezna 1996
