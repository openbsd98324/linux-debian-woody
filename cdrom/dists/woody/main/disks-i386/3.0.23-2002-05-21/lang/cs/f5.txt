0fDAL�� SPECI�LN� SPOU�T�C� PARAMETRY PRO HARDWARE07

Na p��kazov�m ��dku 0fboot:07 lze pou��t n�sleduj�c� parametry v
kombinaci se spou�t�c�mi metodami z <09F307>.
Hexadecim�ln� ��sla je nutn� uv�st p�edponou 0f0x07 (nap�. 0f0x30007).


0fHardware                               Zadat parametr07
Monochromatick� monitor                0fmono07
Vypnout framebuffer pro monitor        0fvideo=vga16:off07
IBM PS/1 nebo ValuePoint (disk IDE)    0fhd=0bcylindry0f,0bhlavy0f,0bsektory07
IBM ThinkPad                           0ffloppy=thinkpad07
IBM Pentium Microchannel               0fmca-pentium no-hlt07
Vynechat z autodetekce oblast          0freserve=0biobase0f,0bextent07[0f,0b...07]
Obej�t vadn� FPU (pro star� po��ta�e)  0fno38707
Advanced Power Management              0fapm=on07

0fModuly:07
Ethernetov� ovlada�e za��zen� a ovlada�e propriet�rn�ch rozhran�
CD-ROM se na��taj� jako moduly, tak�e 0fnelze07 zadat jejich parametry
na p��kazov�m ��dku 0fboot:07. Detaily naleznete v Instala�n�m manu�lu.

Syst�m se spust� stiskem <09ENTER07> �i zad�n�m spou�t�c�ch parametr� a <09ENTER07>.
Rejst��k n�pov�dy se zobraz� stiskem kl�vesy <09F107>.




