Du kan placere enhver komprimeret kerne p� denne diskette, og opn� at
den kan opstarte. G�r s�dan:
     S�t kernen op med f�lgende faciliteter indbygget: initrd,
         ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
     Overs�t din kerne med "make bzImage".
     Kopi�r den til "linux" p� opstartsdisketten.
     Skift katalog til opstartsdisketten og k�r ./rdev.sh for at s�tte
         kernen op.
     Hvis du �nsker det, kan du redigere syslinux.cfg for at tilf�je tilvalg
         til "DEFAULT"-linjen, eller inds�tte en "APPEND"-linje med tilvalg,
	 der tilf�jes enhver brugerindtastet linje s�vel som den forvalgte.

Dokumentation at l�se:
    /usr/share/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentatino/ramdisk.txt

Kildetekst:
    De skripter, der opretter denne diskette og de andre Debian opstarts-
    disketter er installeret i /usr/src/boot-floppies/ af pakke boot-floppies.

- Bruce Perens, 12. marts 1996
