
0fAsennusjärjestelmälle tarkoitetut parametrit07

Voit käyttää seuraavia käynnistysparametrejä 0fboot:07 -kehoitteessa,
käynnistystavan kera (katso <09F307>).  16-järjestelmän luvuille on
käytettävä 0x -etuliitettä (esim. 090x30007).

0fParametri  Merkitys07
kieli	       Asennusjärjestelmän käyttämä kieli; jos tätä ei anneta
	       järjestelmä kysyy sitä.
quiet	       Vähäsanaisempi; vähemmän kysymyksiä
verbose	       Monisanaisempi; enemmän kysymyksiä
debug	       Virheenjäljitys; katso viestit tty3:sta.



Paina <09ENTERo7> tai kirjoita käynnistystapa, käynnistysparametrit ja
sitten <09ENTERo7> käynnistääksesi. Paina funktionäppäintä <09F1o7>
saadaksesi ohjetekstien sisällysluettelon.
