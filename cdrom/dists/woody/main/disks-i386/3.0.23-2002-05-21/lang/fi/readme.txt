Voit kopioida mink� tahansa pakatun Linux ytimen t�lle levykkeelle, ja
sen pit�isi k�ynnisty�. Tee seuraavasti:
  Konfiguroi ytimeen seuraavat ominaisuudet (EI moduleina!) : initrd,
    ramdisk, loop, msdos, fat, elf, ext2fs, procfs.
  K��nn� ydin komennolla "make bzImage".
  Kopioi se nimell� "linux" k�ynnistyslevykkeelle.
  Vaihda oletushakemistoksi k�ynnistyslevyke ja suorita ./rdev.sh
  jotta ydin tulee konfiguroitua. 

  Tarvittaessa muokkaa syslinux.cfg lis�t�ksesi parametrej� "DEFAULT"
    riville, tai lis�� "APPEND" -rivi argumenteille jotka lis�t��n 
    k�ytt�j�n kirjoittamiin komentoriveihin oletusarvon lis�ksi.

Ohjeita luettavaksi:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/documentation/ramdisk.txt

L�hdekoodi:
    T�m�n levykkeen ja muut Debian bootstrap -levykkeet luovat
    komentotiedostot asentaa boot-floppies -paketti hakemistoon
    /usr/src/boot-floppies/.

- Bruce Perens, 12-March-1996
- Suomentanut Tapio Lehtonen, 2000-02-01
