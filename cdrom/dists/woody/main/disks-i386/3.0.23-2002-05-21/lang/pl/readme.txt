Mo�esz umie�ci� na tym dysku dowolne j�dro Linuksa i powinno si� za�adowa�.
Aby to zrobi�:
    Skonfiguruj j�dro tak, aby zawiera�o obs�ug� nast�puj�cych podsystem�w:
        initrd, ramdysk, loop, msdos, fat, elf, ext2fs, procfs.
    Skompiluj j�dro przy pomocy "make bzImage".
    Skopiuj je do pliku "linux" na dysku startowym.
    Przejd� do katalogu dysku startowego i uruchom ./rdev.sh aby
       skonfigurowa� j�dro.
    Ewentualnie zmodyfikuj syslinux.cfg i dodaj argumenty do linii "DEFAULT"
       lub dodaj lini� "APPEND" z argumentami jakie maj� zosta� dodane
       do ka�dej komendy wpisanej przez u�ytkownika a tak�e do komendy
       domy�lnej.

Dokumentacja do przeczytania:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

Kod �r�d�owy:
    Skrypty, kt�re tworz� ten dysk i inne dyski startowe Debiana s�
   instalowane w katalogu /usr/src/boot-floppies/ przez pakiet boot-floppies.

- Bruce Perens, 12-March-1996
 - t�umaczenie: Marcin Owsiany 19.11.1999
