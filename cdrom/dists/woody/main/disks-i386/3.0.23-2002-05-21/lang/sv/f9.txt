0fDEBIANS S�NDLISTOR�LP07

Om du har tillg�ng till webben, �r 0fhttp://www.debian.org/07 ett bra st�lle att
b�rja p�, f�lj l�nken markerad 0fPrenumerera p� s�ndlista07 f�r att f� en �versikt
�ver de fler �n 80 listor som finns i Debianprojektet. Ett bekv�mt formul�r f�r
prenumeration finns p� den sidan. Stora delar av webbplatsen finns tillg�nglig
p� svenska.

Om du k�nner till namnet p� listan du �r intresserad av kan du prenumerera
manuellt genom att s�nda e-post till 0fdebian-<lista>-REQUEST@lists.debian.org07
med ordet "0fsubscribe07". Kommunikation med andra prenumeranter p� listan
sker genom adressen 0fdebian-<lista>@lists.debian.org07, d�r <lista> ers�tts
med namnet p� s�ndlistan i fr�ga.

Om du t.ex vill rapportera ett problem, eller beh�ver hj�lp med installationen
av Debianpaketen s�nder du ordet "0fsubscribe07" i ett brev till adressen
0fdebian-user-REQUEST@lists.debian.org07, f�r att sedan s�nda en detaljerad
beskrivning av problemet till 0fdebian-user@lists.debian.org07. Ditt brev kommer att
l�sas av hundratals Debiananv�ndare, och n�gon kommer s�kerligen hj�lpa dig.
Debians svenskspr�kiga s�ndlista heter 0fdebian-user-swedish07.

Tryck <09ENTER07> eller ange startflaggor f�ljt av <09ENTER07> f�r att starta.
Tryck funktionstangenten <09F107> f�r hj�lpindex.

