Du kan kopiera vilken komprimerad linuxk�rna som helst till denna diskett, och
den blir startbar.
G�r s� h�r:
    Konfigurera k�rnan med st�d f�r: initrd, ramdisk, loop, msdos, fat,
	elf, ext2fs, procfs.
    Kompilera k�rnan med kommandot "make bzImage".
    Kopiera den till "linux" p� startdisketten.
    Byt katalog till startdisketten och k�r ./rdev.sh f�r att konfigurera
        k�rnan.
    Om du vill kan du �ven redigera syslinux.cfg f�r att l�gga till flaggor i
        "DEFAULT"-f�ltet, eller l�gga till ett "APPEND"-f�lt med flaggor som
        kopieras till slutet av alla anv�ndarinskrivna kommandon (�ven till
	"DEFAULT").
	
Dokumentation att l�sa:
    /usr/doc/syslinux/syslinux.doc.gz
    "man rdev"
    /usr/src/linux/Documentation/ramdisk.txt

K�llkod:
    Skriptfilerna som skapar denna diskett och alla andra Debianstartdiskar
    har installerats i katalogen /usr/src/boot-floppies av paketet
    boot-floppies.

- Bruce Perens, 12 mars 1996
