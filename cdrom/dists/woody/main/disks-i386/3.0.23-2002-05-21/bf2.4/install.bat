@echo "Due to a bug in loadlin (#142421), booting the bf2.4 flavor"
@echo "from DOS with loadlin doesn't work.  We're sorry."
@echo ..\dosutils\loadlin linux.bin root=/dev/ram initrd=../images-1.44/bf2.4/root.bin disksize=1.44 flavor=bf2.4