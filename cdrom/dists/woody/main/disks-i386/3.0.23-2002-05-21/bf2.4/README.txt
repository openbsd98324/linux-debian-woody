This is an experimental flavor which uses a special version of the
kernel-image-2.4 package.  It contains support for some new hardware
parts which does not exist in the (more stable) other flavors. It
supports more USB hardware, modern IDE controllers, newer network cards and
Ext3 and Reiser filesystems. Compared to the driver set of our main
kernel-image-2.4.x-yz packages, some non-essential drivers have been removed in
order to keep the number of needed floppy disks in a sane range.

If you have unexplainable problems with kernel 2.4, you should use other
flavors. But be warned: if you replace this kernel after installation with one
the standard kernel-image-2.2.* packages, you may not be able to reboot since
most of the standard packages do not contain the necessary a IDE patch.

If you need more new drivers or optimisations for you CPU type, feel free to
install an "official" kernel-image-2.4.x-yz package. This flavor comes with
one rescue floppy, one root and four driver floppies.

See the file 'kernel-config' for the complete list of devices included
in this flavor.

See the top-level README.txt file for a description of what the files
in this directory are for. To get a complete description of what
files are required for various installation methods, see the
Installation Manual.
