
The -safe disk images use the "safe, slow and stupid" version of SYSLINUX.

From SYSLINUX documentation:

"This version may work on some very buggy BIOSes on which SYSLINUX would
otherwise fail."

You should try those images if the usual ones don't boot on your machine.

See the top-level README.txt file for a description of what the files
in this directory are for.  To get a complete description of what
files are required for various installation methods, see the
Installation Manual.
