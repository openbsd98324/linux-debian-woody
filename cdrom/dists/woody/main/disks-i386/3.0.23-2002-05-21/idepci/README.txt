The idepci flavor contains a kernel with only IDE and PCI support.  It
is geared towards modern PCs without SCSI controllers.  If you have
SCSI, you should be using the 'compact' flavor rather than this one.

It includes support for IDE floppies (ATAPI ZIP, LS-120).  It does not
include support for math emulation, sound, joysticks, video4linux, and
other drivers not essential for installation.

People with Promise Ultra66 IDE controllers should be using the
especially patched 'ide' flavor.

In terms of installation, this set of floppies should work just as well
as the regular ones. However, once your system is installed, you may
wish to install the full kernel image package to get access to other
available kernel modules.

See the file 'kernel-config' for the complete list of devices included
in this flavor.

See the top-level README.txt file for a description of what the files
in this directory are for.  To get a complete description of what
files are required for various installation methods, see the
Installation Manual.
