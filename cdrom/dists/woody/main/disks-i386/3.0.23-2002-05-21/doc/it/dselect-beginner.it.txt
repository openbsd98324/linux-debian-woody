
                    Guida a `dselect' per principianti
                    ----------------------------------

         St�phane Bortzmeyer e altri <debian-doc@lists.debian.org>

                 Eugenia Franzoni <eugenia@pluto.linux.it>

                     Riccardo Fabris <frick@linux.it>


-------------------------------------------------------------------------------


Estratto
--------

     Questo documento � un breve tutorial rivolto a coloro che si trovano
     ad usare per la prima volta `dselect', l'interfaccia da console per la
     gestione dei pacchetti Debian.  Va a integrare il Manuale di
     installazione di Debian GNU/Linux 3.0
     (http://www.debian.org/releases/woody/i386/install).


-------------------------------------------------------------------------------


Contenuti
---------

     1.        Introduzione

     2.        Cosa succede appena lanciato `dselect'
     2.1.      "Metodo"
     2.2.      "Aggiorna"
     2.3.      "Seleziona"
     2.4.      "Installa"
     2.5.      "Configura"
     2.6.      "Rimuovi"
     2.7.      "Termina"

     3.        Alcuni suggerimenti in conclusione

     4.        Glossario


-------------------------------------------------------------------------------


1. Introduzione
---------------

     Potrete trovare versioni aggiornate di questo file presso:
     http://www.debian.org/releases/woody/i386/dselect-beginner.

     Questo file contiene documentazione su `dselect' per i nuovi utenti.
     Il suo scopo � esservi d'aiuto a installare con successo Debian, non
     di fornire spiegazioni onnicomprensive.  Quindi, quando userete per la
     prima volta `dselect', vi converr� leggere le schermate di aiuto.

     Se non vedete l'ora di far funzionare Debian prima possibile, bene,
     allora non dovreste usare `dselect' :-) La procedura d'installazione
     di Debian vi consentir� di eseguire `tasksel', con cui potrete
     selezionare dei "task", insiemi di pacchetti che eseguono un certo
     compito, per ottenere ci� che volete.

     `dselect' serve a selezionare quali pacchetti volete installare (al
     momento in Debian 3.0 ci sono circa 8300 pacchetti).  Verr� avviato
     durante l'installazione e, dato che si tratta di uno strumento potente
     e piuttosto complesso, che pu� fare ottime cose se usato bene, ma
     anche arrecare gravi danni al sistema se usato male o in modo non
     attento, � fortemente consigliato farsi un'idea di come funziona prima
     di usarlo effettivamente

     `dselect' vi accompagner� attraverso il processo di installazione dei
     pacchetti passando per le seguenti fasi:

        * Scelta del metodo di accesso da usare.

        * Aggiornamento dell'elenco dei pacchetti disponibili, se
          possibile.

        * Richiesta dei pacchetti da installare.

        * Installazione ed aggiornamento dei pacchetti richiesti.

        * Configurazione dei pacchetti non ancora configurati.

        * Rimozione del software non desiderato.

     A mano a mano che ciascuna fase viene completata con successo,
     `dselect' passa alla successiva.  Affrontatele tutte nel giusto
     ordine, senza saltarne nessuna.

     In alcuni punti di questo documento viene indicato di inizializzare
     una nuova shell.  Linux ha sei sessioni di console, o shell,
     disponibili in qualsiasi momento.  Si pu� passare da una all'altra
     usando le combinazioni di tasti da _Alt Sin-F1_ a _Alt Sin-F6_.
     Quando si passa a una nuova console, basta fare login nella nuova
     shell e continuare.  La console usata dal processo di installazione �
     la prima, o tty1, quindi per tornarci vi baster� usare _Alt Sin-F1_.


-------------------------------------------------------------------------------


2. Cosa succede appena lanciato `dselect'
-----------------------------------------

     Una volta entrati in `dselect' avrete questa schermata:

Debian GNU/Linux `dselect' interfaccia visuale alla gestione dei pacchetti.

 0. [M]etodo    Scegli il metodo di accesso da usare.
 1. [A]ggiorna  Aggiorna, se possibile, l'elenco dei pacchetti disponibili.
 2. [S]eleziona Scegli quali pacchetti vuoi nel tuo sistema.
 3. [I]nstalla  Installa ed aggiorna i pacchetti scelti.
 4. [C]onfigura Configura tutti i pacchetti ancora da configurare.
 5. [R]imuovi   Rimuovi il software non voluto.
 6. [T]ermina   Esci da dselect.

[un po' di altra roba]

     Esaminiamole una ad una:


2.1. "Metodo"
-------------

     Ecco la schermata Metodo:

dselect - lista dei metodi di accesso
  Abbrev.        Descrizione
  cdrom          Installazione da un CD-ROM.
* multi_cd       Installazione da una serie di CD-ROM.
  nfs            Installazione da un server NFS (non ancora montato).
  multi_nfs      Installazione da un server NFS, usando un insieme di CD-ROM (non ancora montato).
  harddisk       Installazione da una partizione di un disco fisso (non ancora montato).
  mounted        Installazione da un filesystem gi� montato.
  multi_mount    Installazione da una partizione gi� montata con contenuti variabili.
  floppy         Installazione da una pila di floppy disk.
  apt            Installazione tramite APT [file,http,ftp]

     Ora dobbiamo dire a `dselect' dove si trovano i pacchetti da usare.
     Ignorate l'ordine in cui appaiono i metodi di accesso.  E` molto
     importante che scegliate il metodo giusto per l'installazione.  E`
     possibile che nell'elenco troviate alcuni metodi in pi� o in meno o
     che siano in ordine diverso, ma non dovete preoccuparvene.
     Nell'elenco che segue descriveremo i vari metodi.

     apt
          Una delle alternative migliori per l'installazione da un mirror
          locale dell'archivio Debian o dalla rete.  Questo metodo usa il
          sistema "apt" (vedete apt(8)) per fare un'analisi completa delle
          dipendenze, perci� tende a installare i pacchetti nell'ordine
          ottimale.

          La configurazione di questo metodo � lineare: potete scegliere un
          numero qualsiasi di locazioni diverse, mischiando e accoppiando
          URL di `file:' (cio� dischi locali o montati via NFS), URL
          `http:' e `ftp:'.  Notate per� che le opzioni HTTP e FTP non
          supportano l'uso di proxy locali ad autenticazione.

          Consultate la pagina di man sources.list(5) per maggiori
          informazioni sul formato del file `/etc/apt/sources.list'.

          Se avete dei server proxy per HTTP o FTP (o per entrambi),
          assicuratevi di aver impostato le rispettive variabili d'ambiente
          `http_proxy' e `ftp_proxy', cosa che potete fare dalla shell
          prima di avviare dselect, dando una cosa del genere:

               # export http_proxy=http://gateway:3128/
               # dselect

     multi_cd
          Questo metodo, piuttosto complesso e potente, � quello
          raccomandato per installare una versione recente di Debian da una
          serie di CD binari.  Ciascun CD dovrebbe contenere informazioni
          sui pacchetti contenuti in esso e in tutti i CD precedenti (nel
          file `Packages.cd').  Quando scegliete per la prima volta questo
          metodo, assicuratevi che il CD-ROM che andrete ad usare non sia
          montato, inserite nel lettore l'ultimo CD di _binari_ (non
          servono i CD dei sorgenti) e rispondete alle domande che vi
          verranno poste:

             * Ubicazione del lettore CD-ROM

             * Conferma che state usando una serie di CD

             * Posizione della distribuzione Debian sul disco, o dischi.

             * [ Possibilmente ] la posizione del (o dei) file Packages

          Una volta aggiornato l'elenco dei pacchetti disponibili e
          selezionati quelli da installare, il metodo multi-cd si
          differenzia dalla procedura normale, dato che dovrete lanciare
          una fase "Installa" per ognuno dei CD che avete.
          Sfortunatamente, per propri limiti intrinseci, `dselect'non vi
          pu� chiedere di inserire il CD successivo ogni volta.  Il metodo
          da seguire per ciascun CD �:

             * Inserire il CD nel lettore CD-ROM.

             * Dal men� principale di dselect, scegliere "Installa".

             * Aspettare finch� dpkg non abbia finito l'installazione da
               questo CD (alla fine potrebbe riferire che l'installazione
               ha avuto successo o, alle volte, che ci sono stati degli
               errori.  Non ve ne preoccupate per ora).

             * Dare [_Invio_] per tornare al men� principale di dselect.

             * Ripetere il tutto col CD successivo della serie...

          Potrebbe essere necessario ripetere la fase di installazione pi�
          di una volta per fare in modo che i pacchetti vengano installati
          nell'ordine corretto: alcuni pacchetti potrebbero dover aspettare
          l'installazione di altri pacchetti da un CD successivo per poter
          essere configurati correttamente.

          Si raccomanda di attivare "Configura" per configurare tutti i
          pacchetti eventualmente rimasti non configurati.

     multi_nfs, multi_mount
          Sono molto simili al metodo multi_cd.  Si tratta di metodi basati
          anch'essi sulla copia da supporti a contenuto variabile, con
          ulteriori raffinatezze.  Ad esempio installare da una serie di cd
          multipli esportata via NFS dal lettore CD-ROM di un'altra
          macchina.

     floppy
          Un metodo fornito per chi non ha accesso alla rete n� CD-ROM.
          Non � pi� raccomandato se state usando dei dischetti
          tradizionali, ma pu� funzionare decentemente per i dischetti
          LS/120 o Zip.  Specificate la posizione del lettore dei dischetti
          e cominciate ad inserirli.  Il primo dischetto dovrebbe contenere
          il file Packages.  Questo metodo � lento e non totalmente
          affidabile a causa di problemi intrinseci dei supporti.

     nfs
          _METODO DEPRECATO, al suo posto usate apt o multi_nfs.  Da
          utilizzare solo ove tutti gli altri metodi fallissero!_

          E` un metodo di installazione semplice, con requisiti semplici: �
          sufficiente fornire l'indirizzo del server NFS, l'ubicazione
          della distribuzione Debian e del (o dei) file Packages sul
          server, eventualmente.  `dselect' installer� quindi dal server le
          varie sezioni a turno.  Lento ma tranquillo.  Dato che non �
          capace di procedere nell'ordine opportuno da solo, dovrete
          ripetere pi� volte i passi "Installa" e "Configura".  Ovviamente
          � adatto solo alle installazioni via NFS.

     harddisk
          _METODO DEPRECATO, al suo posto usate apt o multi_mount.  Da
          utilizzare solo ove tutti gli altri metodi fallissero._

          Fornitegli il nome della partizione del disco da usare e, come al
          solito, la posizione dei file di Debian al suo interno.  E` un
          metodo lento ma semplice.  Dato che non � capace di procedere nel
          giusto ordine da solo, dovrete rieseguire pi� di una volta i
          passi "Installa" e "Configura".  Vi si raccomanda di non usarlo,
          dato che il metodo "apt" supporta le stesse funzionalit� ed � in
          grado di procedere da s� nell'ordine corretto.

     mounted
          _METODO DEPRECATO, al suo posto usate apt o multi_mount.  Da
          utilizzare solo ove tutti gli altri metodi fallissero._

          Dovrete semplicemente specificare la posizione (o le posizioni)
          dei file di Debian nel vostro file system.  Probabilmente � il
          metodo pi� comodo, ma � piuttosto lento.  Dato che non � capace
          di procedere nel giusto ordine da solo, dovrete eseguire pi� di
          una volta i passi "Installa" e "Configura".

     cdrom
          _METODO DEPRECATO, al suo posto usate multi_cd, dato che non
          funziona con le serie di CD multipli, come quelle incluse in
          Debian 3.0._

          Progettato per installazioni da CD singoli, questo metodo
          elementare vi chieder� l'ubicazione del lettore CD-ROM, della
          distribuzione Debian sul CD e, se necessario, del (o dei) file
          Packages.  Semplice ma piuttosto lento.  Dato che non � capace di
          procedere nel giusto ordine da solo, dovrete ripetere pi� volte i
          passi "Installa" e "Configura".  Non � raccomandato poich�
          presuppone che la distribuzione stia su un solo CD-ROM, cosa che
          non � pi� vera da molto tempo.  Al suo posto usate il metodo
          "multi-cd".

     Se incappate in qualche problema (per dire, Linux non vede il vostro
     CD-ROM, il mount NFS non funziona o avete dimenticato in quale
     partizione si trovano i pacchetti), avete un paio di possibilit�:

        * Inizializzare un'altra shell, correggere il problema e ritornare
          alla shell principale.

        * Chiudere `dselect' e riavviarlo pi� tardi.  Potreste perfino aver
          bisogno di spegnere l'elaboratore per risolvere certi problemi.
          E` accettabile, ma quando tornerete a `dselect' eseguitelo da
          root.  Andr� fatto manualmente, dato che `dselect' non verr�
          eseguito automaticamente dopo la prima volta.

     Dopo aver scelto il metodo di accesso, `dselect' vi chieder� di
     indicare l'ubicazione esatta dei pacchetti.  Se non riuscite a
     inserirla correttamente la prima volta, digitate _Control-C_ e tornate
     alla voce "Metodo".

     Una volta finito, sarete riportati alla schermata principale.


2.2. "Aggiorna"
---------------

     `dselect' legger� i file `Packages' o `Packages.gz' dal mirror e
     creer� sul vostro sistema un database di tutti i pacchetti
     disponibili.  Scaricare ed elaborare i file prender� del tempo.


2.3. "Seleziona"
----------------

     Tenetevi stretti, qui � dove succede il meglio.  Lo scopo di questa
     fase � infatti scegliere i pacchetti che desiderate installare.

     Premete _Invio_.  Se avete una macchina lenta, sappiate che lo schermo
     si ripulir� (potrebbe rimanere oscurato anche per una quindicina di
     secondi), quindi non cominciate a picchiare sui tasti e abbiate
     pazienza.

     La prima cosa che apparir� sullo schermo sar� la prima pagina del file
     di aiuto.  Potete richiamarlo in ogni momento dalle schermate di
     "Seleziona" premendo _?_.  Potete scorrere le pagine con il tasto _._
     (punto).

     Siate pronti a doverci perdere un'oretta o gi� di l�, dato che dovrete
     sbatterci un po' la testa per capire bene come funziona.  Arrivando
     per la prima volta alla schermata "Seleziona", non fate _NESSUNA_
     selezione, premete solo _Invio_ e state a vedere che problemi di
     dipendenze ci sono.  Tentate di risolverli.  Se a un certo punto vi
     ritrovate di nuovo alla schermata principale, ritentate con Sezione
     2.3, `"Seleziona"'.

     Prima di buttarvi, tenete presente che:

        * Per uscire dalla schermata "Seleziona" dopo che avrete finito con
          le selezioni, premete _Invio_.  Se non ci sono problemi con ci�
          che avete fatto, verrete riportati alla schermata principale,
          altrimenti vi verr� chiesto di risolverli.  Quando avete finito
          con una schermata, premete _Invio_ per uscire.

        * E` normale avere qualche problema, non abbiatene timore.  Se
          selezionate il pacchetto <A>, che richiede il pacchetto <B>,
          `dselect' vi avviser� del problema e suggerir� una soluzione.  Se
          il pacchetto <A> va in conflitto con <B> (cio� se si escludono a
          vicenda) vi verr� chiesto di scegliere tra i due.

     Diamo un'occhiata alle due prime righe della schermata "Seleziona".

dselect - lista principale (dispon., priorit�)  marc:+/=/- estesa:v aiuto:?
EIOM Pri Sezione  Pacchetto    Ver.inst.    Ver.disp.   Descrizione

     L'intestazione ci ricorda alcuni dei tasti speciali:

     `+'
          Seleziona un pacchetto da installare.

     `='
          Pone a "Bloccato" un pacchetto, opzione utile con quelli che non
          funzionano: potete reinstallare una versione precedente e
          metterla in "Bloccato" mentre aspettate che ne esca una nuova
          (sono comunque cose che accadono raramente con le release stabili
          di Debian).

     `-' Rimuove un pacchetto.
     `_'
          Rimuove un pacchetto ed i suoi file di configurazione ("Purge").

     `i,I'
          Attiva (_i_) o disattiva (_I_) la visualizzazione di informazioni
          aggiuntive (nella parte bassa dello schermo).

     `o,O'
          Visualizza la lista dei pacchetti secondo le diverse opzioni di
          ordinamento (nella parte alta dello schermo).

     `v,V'
          Passa da una visualizzazione prolissa a una sintetica e
          viceversa.  Usandoli vedrete apparire o scomparire i significati
          delle lettere EIOM sulla seconda riga.  Eccone comunque un
          sommario:

               Flag   Significato              Valori possibili
               E      Errore                   Spazio, R, I
               I      Stato di installazione   Spazio, *, -, U, C, I
               O      Indicazione precedente   *, -, =, _, n
               M      Indicazione attuale      *, -, =, _, n

          (Notate che le maiuscole e le minuscole hanno effetti alquanto
          diversi).

     In questo documento non ne verranno spiegati i significati, piuttosto
     fate riferimento alle schermate di aiuto, in cui � spiegato tutto.
     Facciamo giusto un esempio:

     Entrando in `dselect', troverete una riga del genere:

EIOM Pri  Sezione  Pacchetto  Descrizione
  ** Opz  misc     loadlin    a loader (running under DOS) for LINUX kernel

     Vuol dire che il pacchetto loadlin era stato selezionato l'ultima
     volta che avete eseguito `dselect', che � ancora selezionato, ma non �
     installato.  Perch� no?  La risposta dev'essere che il pacchetto non �
     fisicamente disponibile, forse manca dal mirror.

     Le informazioni che `dselect' usa per installare correttamente tutti i
     pacchetti sono contenute nei file Packages (quelli che avete scaricato
     con [A]ggiorna).  Questi file sono generati a partire dai pacchetti
     stessi.

     Niente � perfetto a questo mondo, pu� talvolta capitare che le
     dipendenze incluse nel pacchetto siano sbagliate, causando cos� un
     problema che `dselect' non � in grado di risolvere.  In ogni caso
     potete uscire da un circolo vizioso del genere usando i comandi _Q_ e
     _X_.

     _Q_
          Un comando per imporsi sul sistema delle dipendenze.  Forza
          `dselect' a ignorare le dipendenze del pacchetto e fare comunque
          quello che gli dite.  Il risultato, naturalmente, potrebbe essere
          tragico...

     _X_
          Usate _X_ se vi siete completamente persi: riporta le cose allo
          stato precedente ed esce.

     I tasti che vi possono aiutare a _non perdervi_ (!) sono _R_, _U_ e
     _D_.

     _R_
          Cancella tutte le selezioni di questo livello; non influisce su
          quelle fatte al livello precedente.

     _U_
          Se `dselect' ha proposto dei cambiamenti e voi ne avete aggiunti
          altri, _U_ ripristiner� le selezioni di `dselect'.

     _D_
          Elimina le selezioni fatte da `dselect', lasciando solo le
          vostre.

     Segue un esempio.  Il pacchetto `xmms' (lo abbiamo scelto perch� ha un
     sacco di dipendenze) dipende dai seguenti pacchetti:

        * `libc6'

        * `libglib1.2'

        * `libgtk1.2'

        * `xlibs'

     Dovrebbero essere installanti anche i seguenti pacchetti, anche se non
     sono essenziali:

        * `libaudiofile0'

        * `libesd0'

        * `libgl1'

        * `libmikmod2'

        * `libogg0'

        * `libvorbis0'

        * `libxml1'

        * `zlib1g'

     Quindi, selezionando `xmms' avrete una schermata come segue:

dselect - lista ricorsiva dei pacchetti             marc:+/=/- estesa:v aiuto:?
EIOM Pri Sezione  Pacchetto    Descrizione
  _* Opz sound    xmms         Versatile X audio player that looks like Winamp
  _* Opz libs     libglib1.2   The GLib library of C routines
  _* Opz libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  _* Opz libs     libmikmod2   A portable sound library
  _* Opz libs     libogg0      Ogg Bitstream Library
  _* Opz libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     (Potranno comparire o meno altri pacchetti, a seconda di ci� che � gi�
     installato sul vostro sistema).  Noterete che tutti i pacchetti
     richesti sono gi� stati automaticamente selezionati, assieme a quelli
     raccomandati.

     Il tasto _R_ riporta le cose allo stato iniziale.

dselect - lista ricorsiva dei pacchetti             marc:+/=/- estesa:v aiuto:?
EIOM Pri Sezione  Pacchetto    Descrizione
  __ Opz sound    xmms         Versatile X audio player that looks like Winamp
  __ Opz libs     libglib1.2   The GLib library of C routines
  __ Opz libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  __ Opz libs     libmikmod2   A portable sound library
  __ Opz libs     libogg0      Ogg Bitstream Library
  __ Opz libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Ora per stabilire che non volete installare il pacchetto `xmms' vi
     baster� premere _Invio_.

     Il tasto _D_ rimette le cose secondo quanto selezionato all'inizio:

dselect - lista ricorsiva dei pacchetti             marc:+/=/- estesa:v aiuto:?
EIOM Pri Sezione  Pacchetto    Descrizione
  _* Opz sound    xmms         Versatile X audio player that looks like Winamp
  __ Opz libs     libglib1.2   The GLib library of C routines
  __ Opz libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  __ Opz libs     libmikmod2   A portable sound library
  __ Opz libs     libogg0      Ogg Bitstream Library
  __ Opz libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Il tasto _U_ ripristina le selezioni di `dselect':

dselect - lista ricorsiva dei pacchetti             marc:+/=/- estesa:v aiuto:?
EIOM Pri Sezione  Pacchetto    Descrizione
  _* Opz sound    xmms         Versatile X audio player that looks like Winamp
  _* Opz libs     libglib1.2   The GLib library of C routines
  _* Opz libs     libgtk1.2    The GIMP Toolkit set of widgets for X
  _* Opz libs     libmikmod2   A portable sound library
  _* Opz libs     libogg0      Ogg Bitstream Library
  _* Opz libs     libvorbis0   The OGG Vorbis lossy audio compression codec.

     Vi consigliamo di lasciare i default per adesso, avrete ampie
     opportunit� di aggiungere altro in seguito.

     Qualsiasi cosa decidiate di fare, premete _Invio_ per accettare le
     modifiche e tornare alla schermata principale.  Se le selezioni
     portano a dei problemi, verrete rimandati subito ad una schermata di
     risoluzione dei conflitti.

     I tasti _R_, _U_ e _D_ sono molto utili anche per simulare
     l'installazione di qualche pacchetto.  Potete fare esperimenti,
     ripristinare le selezioni originali e ricominciare.  _Non_
     considerateli come fossero in una di quelle scatole di vetro con su
     scritto "Rompere in caso di emergenza".

     Dopo aver compiuto le vostre selezioni nella schermata "Seleziona",
     premete il tasto _I_ per ottenere una finestra intera, _t_ per
     portarvi all'inizio dell'elenco e usate _Pagina Gi�_ per controllare
     velocemente le impostazioni.  In questo modo potrete controllare i
     risultati del vostro lavoro e distinguere subito gli errori evidenti.
     Vi potrebbe capitare di aver deselezionato per errore interi gruppi di
     pacchetti e di non notare lo sbaglio fino a quando ormai � troppo
     tardi.  `dselect' � uno strumento _molto_ potente, meglio quindi farne
     buon uso.

     Ora dovreste trovarvi in questa situazione:

          categoria del pacchetto   stato
          
          essenziale                tutti selezionati
          importante                tutti selezionati
          standard                  gran parte selezionati
          opzionale                 gran parte selezionati
          extra                     gran parte selezionati

     Contenti?  Premete _Invio_ per uscire.  Potrete tornare in "Seleziona"
     in qualsiasi momento.


2.4. "Installa"
---------------

     `dselect' scorre l'intero insieme dei 8300 pacchetti e installa quelli
     selezionati.  Aspettatevi che vi vengano poste delle domande su
     decisioni da prendere durante l'operazione.

     Lo scorrimento sar� piuttosto rapido su una macchina veloce.  Potrete
     fermarlo con _Control-s_ e riavviarlo con _Control-q_.  Alla fine vi
     verr� mostrata una lista di eventuali pacchetti non installati.  Se
     volete registrare tutto ci� che accade, usate i normali strumenti Unix
     per la cattura dell'output, come tee(1) o script(1).

     Pu� accadere che un pacchetto non venga installato perch� dipende da
     qualche altro pacchetto che, pur comparendo nella lista di quelli da
     installare, non � stato ancora installato.  In tal caso conviene
     lanciare di nuovo "Installa".  Ci sono stati dei casi in cui � stato
     necessario farlo per ben 4 volte prima che tutto andasse a posto,
     anche a seconda dal metodo usato per l'installazione.  Se userete il
     metodo APT, ad esempio, non sar� quasi mai necessario eseguire pi�
     volte "Installa".


2.5. "Configura"
----------------

     La maggior parte dei pacchetti viene configurata nel passo 3, ma
     qualcosa � rimasto indietro viene configurato qui.


2.6. "Rimuovi"
--------------

     Elimina i pacchetti installati che non servono pi�.


2.7. "Termina"
--------------

     Esce da `dselect'.

     Vi suggeriamo di far girare `/etc/cron.daily/find' a questo punto,
     dato che avrete molti nuovi file sul sistema.  In tal modo potrete poi
     usare `locate' per trovarli per nome.


-------------------------------------------------------------------------------


3. Alcuni suggerimenti in conclusione
-------------------------------------

     Potete farvi un'idea delle dimensioni di un pacchetto premendo _i_ due
     volte e dando un'occhiata a "Size".  E` la dimensione del pacchetto
     compresso, quindi una volta decompresso occuper� parecchio spazio in
     pi� (per sapere le dimensioni in KB, date un'occhiata a
     "Installed-Size").

     Installare da zero un sistema Debian � un compito piuttosto complesso,
     ma `dselect' pu� esservi d'aiuto, purch� abbiate la pazienza di
     imparare come farlo funzionare bene.  Leggete le schermate di aiuto e
     fate delle prove con _i, I, o,_ e _O_.  Usate il tasto _R_ quando
     serve.  Tutto qui, sta a voi usarlo in modo efficace.


-------------------------------------------------------------------------------


4. Glossario
------------

     I termini qui riportati sono utili per la comprensione del documento
     e, in generale, quando si parla di Debian.

     Pacchetto (in inglese "package" NdT)
          Un file che contiene tutto il necessario per installare ed
          utilizzare un particolare programma.

          I nomi dei pacchetti Debian hanno il caratteristico suffisso
          <.deb>.  Ogni pacchetto ha un nome e una versione.  La versione �
          composta dalla versione in circolazione (o ufficiale, "upstream")
          e dalla revisione Debian, separate da un trattino ("-").

          Ecco qualche esempio di nomi di pacchetti:

             * `efax_08a-1.deb'

             * `lrzsz_0.12b-1.deb'

             * `mgetty_0.99.2-6.deb'

             * `minicom_1.75-1.deb'

             * `term_2.3.5-5.deb'

             * `uucp_1.06.1-2.deb'

             * `uutraf_1.1-1.deb'

             * `xringd_1.10-2.deb'

             * `xtel_3.1-2.deb'

     dpkg
          Il programma che gestisce i pacchetti si chiama `dpkg'.
          `dselect' � un'interfaccia a `dpkg'.  Gli utenti esperti spesso
          preferiscono usare `dpkg' per installare o rimuovere un
          pacchetto, dato che � pi� veloce.

     Script di pacchetto, script del manutentore (o responsabile)
          Si tratta dei programmi (di solito script di shell) che `dpkg'
          lancia prima e dopo l'installazione di ogni pacchetto.  Di solito
          la loro esecuzione � silenziosa, ma alcuni di essi possono
          visualizzare degli avvisi o porvi delle domande.


-------------------------------------------------------------------------------


     Guida a `dselect' per principianti

     St�phane Bortzmeyer e altri <debian-doc@lists.debian.org>
     Eugenia Franzoni <eugenia@pluto.linux.it>
     Riccardo Fabris <frick@linux.it>


